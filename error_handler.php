<?php

	function elkErrorHandler($errno, $errmsg, $filename, $linenum, $vars) 
	{
		$dt = date("Y-m-d H:i:s (T)");

		$errortype = array (
					E_ERROR              => 'Error',
					E_WARNING            => 'Warning',
					E_PARSE              => 'Parsing Error',
					E_NOTICE             => 'Notice',
					E_CORE_ERROR         => 'Core Error',
					E_CORE_WARNING       => 'Core Warning',
					E_COMPILE_ERROR      => 'Compile Error',
					E_COMPILE_WARNING    => 'Compile Warning',
					E_USER_ERROR         => 'User Error',
					E_USER_WARNING       => 'User Warning',
					E_USER_NOTICE        => 'User Notice',
					E_STRICT             => 'Runtime Notice',
					E_RECOVERABLE_ERROR  => 'Catchable Fatal Error',
					E_DEPRECATED         => 'Deprecated'
					);
		$user_errors = array(E_USER_ERROR, E_USER_WARNING, E_USER_NOTICE);

		if($errortype[$errno] != 'Deprecated') {

			//echo '<pre>';
			$err = "<errorentry>\n";
			$err .= "\t<datetime>" . $dt . "</datetime>\n";
			$err .= "\t<errornum>" . $errno . "</errornum>: ";
			$err .= "[<errortype>" . $errortype[$errno] . "</errortype>] -> ";
			$err .= "<errormsg>" . $errmsg . "</errormsg>\n";
			$err .= "\tIn: <scriptname>" . $filename . "</scriptname> (line: ";
			$err .= "<scriptlinenum>" . $linenum . "</scriptlinenum>)\n";

			if (in_array($errno, $user_errors)) {
				$err .= "\t<vartrace>" . wddx_serialize_value($vars, "Variables") . "</vartrace>\n";
			}
			$err .= "</errorentry>\n\n";

			//echo $err;
			//echo '</pre>';
		}
		return true;
	}

	error_reporting(E_ALL ^ E_DEPRECATED);
	$old_error_handler = set_error_handler("elkErrorHandler");

?>