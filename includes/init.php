<?php

     /*** include the controller class ***/
     include 'application/Controller_base.class.php';

     /*** include the controller class ***/
     include 'application/Model_base.class.php';

     /*** include the registry class ***/
     include 'application/Registry.class.php';

     /*** include the router class ***/
     include 'application/Router.class.php';

     /*** include the template class ***/
     include 'application/Template.class.php';

     /*** include the config class ***/
     include 'application/Config.class.php';

     /*** include the template class ***/
     include 'application/Db.class.php';

     /*** a new registry object ***/
     $registry = new registry;

     /*** host (0: host = localehost)***/
     $host = 0;

     /*** a new config object ***/
     $config = new config($host);

     /*** create the database registry object ***/
     $registry->db = db::getInstance($config->dbName, $config->dbUser, $config->dbPass);


?>
