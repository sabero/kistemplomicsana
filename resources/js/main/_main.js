"use strict";

var THEMEREX_isotope_resize_delta = 0.3;
var THEMEREX_ADMIN_MODE = false;
var THEMEREX_error_msg_box = null;
var THEMEREX_video_resize_inited = false;
var THEMEREX_top_height = 0;
var THEMEREX_use_fixed_wrapper = true;
var THEMEREX_MESSAGE_BOOKMARK_ADD = "Add the bookmark";
var THEMEREX_MESSAGE_BOOKMARK_ADDED = "Current page has been successfully added to the bookmarks. You can see it in the right panel on the tab \'Bookmarks\'";
var THEMEREX_MESSAGE_BOOKMARK_TITLE = "Enter bookmark title";
var THEMEREX_MESSAGE_BOOKMARK_EXISTS = "Current page already exists in the bookmarks list";
var THEMEREX_MESSAGE_EMAIL_CONFIRM = "On the e-mail address <b>%s</b> we sent a confirmation email.<br>Please, open it and click on the link.";
var THEMEREX_MESSAGE_EMAIL_ADDED = "Your address <b>%s</b> has been successfully added to the subscription list";
var THEMEREX_MAGNIFIC_LOADING = "Loading image #%curr% ...";
var THEMEREX_MAGNIFIC_ERROR = "<a href=\"%url%\">The image #%curr%</a> could not be loaded.";
var THEMEREX_NAME_EMPTY = "A név nem lehet üres";
var THEMEREX_NAME_LONG = "A megadott név túl hosszú";
var THEMEREX_TITLE_EMPTY = "A cím nem lehet üres";
var THEMEREX_TITLE_LONG = "A megadott cím túl hosszú";
var THEMEREX_TEXT_EMPTY = "A leírás vagy tartalom nem lehet üres";
var THEMEREX_TEXT_LONG = "A megadott leírás vagy tartalom túl hosszú";
var THEMEREX_LINK_EMPTY = "A link nem lehet üres";
var THEMEREX_LINK_LONG = "A megadott link túl hosszú";
var THEMEREX_EMAIL_EMPTY = "A megadott e-mail cím túl rövid vagy üres";
var THEMEREX_EMAIL_LONG = "A megadott e-mail cím túl hosszú";
var THEMEREX_EMAIL_NOT_VALID = "A megadott e-mail cím érvénytelen";
var THEMEREX_SUBJECT_EMPTY = "A megadott tárgy nem lehet üres";
var THEMEREX_SUBJECT_LONG = "A megadott tárgy túl hosszú";
var THEMEREX_MESSAGE_EMPTY = "A megadott üzenet nem lehet üres";
var THEMEREX_MESSAGE_LONG = "A megadott üzenet túl hosszú";
var THEMEREX_SEND_COMPLETE = "Üzenetét sikeresen továbbítottuk!";
var THEMEREX_SEND_ERROR = "Az üzenet küldése sikertelen, kérjük próbálkozzon ismét!";
var THEMEREX_ajax_url = "#";
var THEMEREX_ajax_nonce = "b700d01c81";

var THEMEREX_PWD_EMPTY = "A megadott jelszó nem lehet üres";
var THEMEREX_PWD_LONG = "A megadott jeszó túl hosszú";
var THEMEREX_USER_NAME_EMPTY = "A megadott név túl rövid vagy üres";
var THEMEREX_USER_NAME_LONG = "A megadott név túl hosszú";
var THEMEREX_PWDS_NOT_EQUAL = "A két jelszó nem egyezik";

// Site base url
var THEMEREX_site_url = "#";

// Theme base font
var THEMEREX_theme_font = "";

// Theme skin
var THEMEREX_theme_skin = "kinder";
var THEMEREX_theme_skin_bg = "#ffffff";

// Slider height
var THEMEREX_slider_height = 500;

// Sound Manager
var THEMEREX_sound_enable = true;
var THEMEREX_sound_folder = 'sounds/';
var THEMEREX_sound_mainmenu = 'sounds/l9.mp3';
var THEMEREX_sound_othermenu = 'sounds/l2.mp3';
var THEMEREX_sound_buttons = 'sounds/mouseover3.mp3';
var THEMEREX_sound_links = 'sounds/l6.mp3';
var THEMEREX_sound_state = {
    all: THEMEREX_sound_enable ? 1 : 0,
    mainmenu: 0,
    othermenu: 0,
    buttons: 1,
    links: 0
};

// System message
var THEMEREX_systemMessage = {
    message: "",
    status: "",
    header: ""
};

// User logged in
var THEMEREX_userLoggedIn = false;

// Show table of content for the current page
var THEMEREX_menu_toc = 'float';
var THEMEREX_menu_toc_home = THEMEREX_menu_toc != 'no' && true;
var THEMEREX_menu_toc_top = THEMEREX_menu_toc != 'no' && true;

// Fix main menu
var THEMEREX_menuFixed = true;

// Use responsive version for main menu
var THEMEREX_menuResponsive = 1024;
var THEMEREX_responsive_menu_click = true;

// Right panel demo timer
var THEMEREX_demo_time = 5000;

// Video and Audio tag wrapper
var THEMEREX_useMediaElement = true;

// Use AJAX search
var THEMEREX_useAJAXSearch = true;
var THEMEREX_AJAXSearch_min_length = 3;
var THEMEREX_AJAXSearch_delay = 200;

// Popup windows engine
var THEMEREX_popupEngine = 'magnific';
var THEMEREX_popupGallery = true;

// E-mail mask
var THEMEREX_EMAIL_MASK = '^([a-zA-Z0-9_\\-]+\\.)*[a-zA-Z0-9_\\-]+@[a-z0-9_\\-]+(\\.[a-z0-9_\\-]+)*\\.[a-z]{2,6}$';

// Messages max length
var THEMEREX_msg_maxlength_contacts = 1000;
var THEMEREX_msg_maxlength_comments = 1000;

// Remember visitors settings
var THEMEREX_remember_visitors_settings = true;

if (THEMEREX_theme_font == '') THEMEREX_theme_font = 'Roboto Slab';

// Add skin custom colors in custom styles
function theme_skin_set_theme_color(custom_style, clr) {
    custom_style +=
        '.topWrap .topMenuStyleLine ul#mainmenu .menu-panel .item_placeholder .item_title a:hover,.topWrap .topMenuStyleLine ul#mainmenu .menu-panel.thumb .item_placeholder .item_title a:hover' + ' { color: ' + clr + ' !important; }' + '.sliderHomeBullets .order a,.usermenu_area,.twitBlock,.twitBlockWrap,.twitBlock .sc_slider .flex-direction-nav li' + ' { background-color: ' + clr + '; }' + '.topWrap .openRightMenu:hover,.topWrap .search:not(.searchOpen):hover,.sliderHomeBullets .order a' + ' {border-color: ' + clr + '; }';
    return custom_style;
}

// Add skin's main menu (top panel) back color in the custom styles
function theme_skin_set_menu_bgcolor(custom_style, clr) {
    return custom_style;
}

// Add skin top panel colors in custom styles
function theme_skin_set_menu_color(custom_style, clr) {
    custom_style +=
        '.responsive_menu .menuTopWrap > ul > li > a,.responsive_menu .menuTopWrap li.menu-item-has-children:before' + ' { color: ' + clr + '; }';
    return custom_style;
}

// Add skin's user menu (user panel) back color in the custom styles
function theme_skin_set_user_menu_bgcolor(custom_style, clr) {
    return custom_style;
}

// Add skin's user menu (user panel) fore colors in the custom styles
function theme_skin_set_user_menu_color(custom_style, clr) {
    return custom_style;
}

jQuery(document).ready(function() {
    userpopup_menu();
    userpopup_logout();
    userpopup_delete();
    userpopup_admin_menu();
    timelineResponsive();
    ready();
    timelineScrollFix();
    itemPageFull();
    mainMenuResponsive();
    scrollAction();
    calcMenuColumnsWidth();
    resizeVideoBackground();
    REX_parallax();
    //isotope_filtr();
    customizer_scroll();
    hover_mobile();
    review_star();
    slider_range();
    shop_calculator();
    swiper_slider_init();
    swiper_multi_slider();
    toTop();
    massage_init();
    royalSlider_init();
    // Resize handlers
    jQuery(window).resize(function() {
        timelineResponsive();
        fullSlider();
        resizeSliders();
        itemPageFull();
        mainMenuResponsive();
        scrollAction();
        resizeVideoBackground();
        REX_parallax()
        hover_mobile();
        swiper_slider_init();
        swiper_multi_slider();
        royalSlider_init();
    });
    // Scroll handlers
    jQuery(window).scroll(function() {
        timelineScrollFix();
        scrollAction();
        REX_parallax()
    });
    // Scroll handlers
    jQuery(window).load(function() {
        preloader();
        swiper_slider_init();
        swiper_multi_slider();
    })
});


// riyalSlider init
function royalSlider_init() {
    if (jQuery(".royalSlider").length > 0) {
        $(".royalSlider").royalSlider({
            // general options go gere
            autoScaleSlider: true,
            loop: true,
            block: {
                // animated blocks options go gere
                fadeEffect: false,
                moveEffect: 'left'
            },
            autoPlay: {
                // autoplay options go gere
                enabled: true,
                delay: 7000,
                stopAtAction: true,
                pauseOnHover: true
            }
        });
    }
}

// Preloader 
function preloader() {
    jQuery("#preloader_image").fadeOut();
    jQuery("#preloader").delay(200).fadeOut("slow").delay(200, function() {
        jQuery(this).remove();
    });
}

// Init swiper sliders
function swiper_slider_init() {

    if (jQuery("#swiper_container_1").length > 0) {
        var swiper1 = new Swiper('#swiper_container_1', {
            slidesPerView: 1,
            spaceBetween: 0,
            grabCursor: true,
            loop: true,
            autoplay: 4000
        });
    }

    if (jQuery("#swiper_container_2").length > 0) {
        var swiper2 = new Swiper('#swiper_container_2', {
            nextButton: '.swiper-button-next',
            prevButton: '.swiper-button-prev',
            pagination: '.swiper-pagination',
            slidesPerView: 1,
            paginationClickable: true,
            spaceBetween: 0,
            grabCursor: true,
            loop: true,
            autoplay: 4000
        });
    }

    if (jQuery("#swiper_container_3").length > 0) {
        var swiper3 = new Swiper('#swiper_container_3', {
            nextButton: '.swiper-button-next3',
            prevButton: '.swiper-button-prev3',
            slidesPerView: 1,
            spaceBetween: 0,
            grabCursor: true,
            loop: true,
            autoplay: 4000
        });
    }

    if (jQuery("#swiper_container_4").length > 0) {
        var swiper4 = new Swiper('#swiper_container_4', {
            scrollbar: '.swiper-scrollbar_tab4',
            scrollbarHide: true,
            direction: 'vertical',
            slidesPerView: 2,
            spaceBetween: 0,
            grabCursor: true,
            mousewheelControl: true,
            freeMode: true
        });
    }
    /*if (jQuery(".swiper-container4").length > 0) {
        $('#tabs_sliders a').on('shown.bs.tab', function(e) {
            if ($(this).attr('href') == "#tab1") {
                var swiper4 = new Swiper('.swiper-container4', {
                    scrollbar: '.swiper-scrollbar_tab4',
                    scrollbarHide: true,
                    direction: 'vertical',
                    slidesPerView: 2,
                    spaceBetween: 0,
                    grabCursor: true,
                    mousewheelControl: true,
                    freeMode: true
                });
            }
        });
    }*/

    if (jQuery("#swiper_container_5").length > 0) {
        $('#tabs_sliders a').on('shown.bs.tab', function(e) {
            if ($(this).attr('href') == "#tab22") {
                var swiper5 = new Swiper('#swiper_container_5', {
                    scrollbar: '.swiper-scrollbar_tab5',
                    scrollbarHide: true,
                    direction: 'vertical',
                    slidesPerView: 2,
                    spaceBetween: 0,
                    grabCursor: true,
                    mousewheelControl: true,
                    freeMode: true
                });
            }
        });
    }

    if (jQuery("#swiper_container_6").length > 0) {
        var swiper8 = new Swiper('#swiper_container_6', {
            nextButton: '.flex-next',
            prevButton: '.flex-prev',
            slidesPerView: 1,
            spaceBetween: 0,
            loop: true,
            grabCursor: true,
            autoplay: 4000
        });
    }

    if (jQuery("#swiper_container_6_2").length > 0) {
        var swiper8 = new Swiper('#swiper_container_6_2', {
            nextButton: '.flex-next',
            prevButton: '.flex-prev',
            slidesPerView: 1,
            spaceBetween: 0,
            loop: true,
            grabCursor: true,
            autoplay: 4000
        });
    }  
  
    if (jQuery("#swiper_container_7").length > 0) {
        var swiper9 = new Swiper('#swiper_container_7', {
            nextButton: '.flex-next',
            prevButton: '.flex-prev',
            slidesPerView: 1,
            spaceBetween: 0,
            grabCursor: true,
            loop: true,
            autoplay: 4000
        });
    }

    if (jQuery(".swiper-container_8").length > 0) {
        var swiper10 = new Swiper('.swiper-container_8', {
            scrollbar: '.swiper-scrollbar',
            scrollbarHide: true,
            nextButton: '.swiper-button-next8',
            prevButton: '.swiper-button-prev8',
            direction: 'vertical',
            slidesPerView: 3,
            spaceBetween: 0,
            mousewheelControl: true,
            grabCursor: true,
            freeMode: true
        });
    }

    if (jQuery("#swiper_container_9").length > 0) {
        $('#tabs_sliders a').on('shown.bs.tab', function(e) {
            if ($(this).attr('href') == "#tab2") {
                var swiper9 = new Swiper('#swiper_container_9', {
                    scrollbar: '.swiper-scrollbar_tab9',
                    scrollbarHide: true,
                    direction: 'vertical',
                    slidesPerView: 2,
                    spaceBetween: 0,
                    grabCursor: true,
                    mousewheelControl: true,
                    freeMode: true
                });
            }
        });
    }

    if (jQuery("#swiper_container_10").length > 0) {
        $('#tabs_sliders a').on('shown.bs.tab', function(e) {
            if ($(this).attr('href') == "#tab3") {
                var swiper6 = new Swiper('#swiper_container_10', {
                    scrollbar: '.swiper-scrollbar_tab10',
                    scrollbarHide: true,
                    direction: 'vertical',
                    slidesPerView: 2,
                    spaceBetween: 0,
                    grabCursor: true,
                    mousewheelControl: true,
                    freeMode: true
                });
            }
        });
    }

    if (jQuery("#swiper_container_11").length > 0) {
        var isSliderActive4 = true;
        $('#tabs_sliders a').on('shown.bs.tab', function(e) {
            if ($(this).attr('href') == "#tab4") {
                var swiper7 = new Swiper('#swiper_container_11', {
                    scrollbar: '.swiper-scrollbar_tab11',
                    scrollbarHide: true,
                    direction: 'vertical',
                    slidesPerView: 2,
                    spaceBetween: 0,
                    mousewheelControl: true,
                    grabCursor: true,
                    freeMode: true
                });
            }
        });
    }    

    if (jQuery("#swiper_container_12").length > 0) {
        var swiper12 = new Swiper('#swiper_container_12', {
            scrollbar: '.swiper-scrollbar12',
            scrollbarHide: true,
            nextButton: '.swiper-button-next12',
            prevButton: '.swiper-button-prev12',
            slidesPerView: 5,
            spaceBetween: 25,
            grabCursor: true,
            freeMode: true
        });
    }

    if (jQuery("#swiper_container_13").length > 0) {
        var swiper13 = new Swiper('#swiper_container_13', {
            scrollbar: '.swiper-scrollbar13',
            scrollbarHide: true,
            nextButton: '.swiper-button-next13',
            prevButton: '.swiper-button-prev13',
            //slidesPerView: 8,
            slidesPerView: 'auto',
            spaceBetween: 25,
            grabCursor: true,
            freeMode: true
        });
    }

    if (jQuery("#swiper_container_14").length > 0) {
        var swiper14 = new Swiper('#swiper_container_14', {
            scrollbar: '.swiper-scrollbar',
            scrollbarHide: true,
            direction: 'vertical',
            slidesPerView: 8,
            spaceBetween: 0,
            mousewheelControl: true,
            grabCursor: true,
            freeMode: true
        });
    }

    /*if (jQuery(".swiper-container_15").length > 0) {
        var swiper15 = new Swiper('.swiper-container_15', {
            scrollbar: '.swiper-scrollbar15',
            scrollbarHide: true,
            direction: 'vertical',
            slidesPerView: 4,
            spaceBetween: 10,
            grabCursor: true,
            freeMode: true
        });
    }*/

    if (jQuery("#swiper_container_16").length > 0) {
        var swiper16 = new Swiper('#swiper_container_16', {
            scrollbar: '.swiper-scrollbar16',
            scrollbarHide: true,
            nextButton: '.swiper-button-next16',
            prevButton: '.swiper-button-prev16',
            direction: 'vertical',
            slidesPerView: 3,
            spaceBetween: 0,
            mousewheelControl: true,
            grabCursor: true,
            freeMode: true
        });
    }

    if (jQuery("#swiper_container_17").length > 0) {
        var swiper17 = new Swiper('#swiper_container_17', {
            nextButton: '.swiper-button-next17',
            prevButton: '.swiper-button-prev17',
            direction: 'vertical',
            slidesPerView: 'auto',
            spaceBetween: 0,
            mousewheelControl: true,
            grabCursor: true,
            freeMode: true
        });
    }

    if (jQuery("#swiper_container_18").length > 0) {
        var swiper11 = new Swiper('#swiper_container_18', {
            scrollbar: '.swiper-scrollbar',
            scrollbarHide: true,
            nextButton: '.swiper-button-next9',
            prevButton: '.swiper-button-prev9',
            direction: 'vertical',
            slidesPerView: 7,
            spaceBetween: 0,
            mousewheelControl: true,
            grabCursor: true,
            freeMode: true
        });
    }
}

// Init swiper multi sliders
function swiper_multi_slider() {

    if (jQuery("#multi_slider").length > 0) {

        var sliderleft = new Swiper('#multi_slider_left', {
            nextButton: '.flex-next',
            prevButton: '.flex-prev',
            grabCursor: true,
            loop: true,
            loopedSlides: 4,
            autoplay: 6000,

        });
        var sliderright = new Swiper('#multi_slider_right', {
            direction: 'vertical',
            centeredSlides: true,
            slidesPerView: 3,
            touchRatio: 0.2,
            grabCursor: true,
            loop: true,
            loopedSlides: 4,
            slideToClickedSlide: true
        });
        sliderleft.params.control = sliderright;
        sliderright.params.control = sliderleft;
    }
}

// Price range slider
function slider_range() {
    if (jQuery("#slider-range").length > 0) {
        $("#slider-range").slider({
            range: true,
            min: 0,
            max: 500,
            values: [0, 500],
            slide: function(event, ui) {
                $("#amount").val("$" + ui.values[0] + " - $" + ui.values[1]);
            }
        });
        $("#amount").val("$" + $("#slider-range").slider("values", 0) +
            " - $" + $("#slider-range").slider("values", 1));
    }
}

// Scroll customizer
function customizer_scroll() {
    $(".swpLeftPos .menuTranform .sc_scroll").mCustomScrollbar({
        autoHideScrollbar: true,
        theme: "dark"
    });
};

// Add Isotope Filtr Button 
function isotope_filtr(galeryList) {

    var galeryMenu = '<ul>';
    for(var i = 0; i <galeryList.length; i++){
        var galeryName = galeryList[i]['name'];
        var galeryFilter = galeryList[i]['filter'];
        galeryMenu = galeryMenu + '<li class="squareButton sb_filter active"><a href="#" data-filter=".'+galeryFilter+'">galeryName</a></li>';
    }
    galeryMenu = galeryMenu + '</ul>';


    //Portfolio masonry columns (CSANA)
    if (jQuery("#style_portfolio_mc").length > 0) {
        jQuery(".isotopeFiltr", "#style_portfolio_mc").append(galeryMenu);
    }

    //Portfolio classic one columns
    if (jQuery("#style_portfolio1").length > 0) {
        jQuery(".isotopeFiltr", "#style_portfolio1").append('<ul><li class="squareButton active"><a href="#" data-filter="*">All</a></li><li class="squareButton"><a href="#" data-filter=".flt_65">clear</a></li><li class="squareButton"><a href="#" data-filter=".flt_36">Design</a></li><li class="squareButton"><a href="#" data-filter=".flt_66">exclusive</a></li><li class="squareButton"><a href="#" data-filter=".flt_7">portfolio</a></li><li class="squareButton"><a href="#" data-filter=".flt_63">portfolio hover</a></li><li class="squareButton"><a href="#" data-filter=".flt_64">print</a></li></ul>');
    }

    //Portfolio classic two columns
    if (jQuery("#style_portfolio2").length > 0) {
        jQuery(".isotopeFiltr", "#style_portfolio2").append('<ul><li class="squareButton active"><a href="#" data-filter="*">All</a></li><li class="squareButton"><a href="#" data-filter=".flt_65">clear</a></li><li class="squareButton"><a href="#" data-filter=".flt_66">exclusive</a></li><li class="squareButton"><a href="#" data-filter=".flt_7">portfolio</a></li><li class="squareButton"><a href="#" data-filter=".flt_36">Design</a></li><li class="squareButton"><a href="#" data-filter=".flt_51">portfolio classic</a></li><li class="squareButton"><a href="#" data-filter=".flt_64">print</a></li><li class="squareButton"><a href="#" data-filter=".flt_10">gallery</a></li></ul>');
    }

    //Portfolio classic four columns
    if (jQuery("#style_portfolio4").length > 0) {
        jQuery(".isotopeFiltr", "#style_portfolio4").append("<ul><li class=\"squareButton active\"><a href=\"#\" data-filter=\"*\">All</a></li><li class=\"squareButton\"><a href=\"#\" data-filter=\".flt_65\">clear</a></li><li class=\"squareButton\"><a href=\"#\" data-filter=\".flt_36\">Design</a></li><li class=\"squareButton\"><a href=\"#\" data-filter=\".flt_66\">exclusive</a></li><li class=\"squareButton\"><a href=\"#\" data-filter=\".flt_7\">portfolio</a></li><li class=\"squareButton\"><a href=\"#\" data-filter=\".flt_63\">portfolio hover</a></li><li class=\"squareButton\"><a href=\"#\" data-filter=\".flt_64\">print</a></li><li class=\"squareButton\"><a href=\"#\" data-filter=\".flt_118\">nature</a></li></ul>");
    }

    //Portfolio classic columns
    if (jQuery("#style_portfolio_cc").length > 0) {
        jQuery(".isotopeFiltr", "#style_portfolio_cc").append('<ul><li class="squareButton active"><a href="#" data-filter="*">All</a></li><li class="squareButton"><a href="#" data-filter=".flt_65">clear</a></li><li class="squareButton"><a href="#" data-filter=".flt_66">exclusive</a></li><li class="squareButton"><a href="#" data-filter=".flt_7">portfolio</a></li><li class="squareButton"><a href="#" data-filter=".flt_36">Design</a></li><li class="squareButton"><a href="#" data-filter=".flt_51">portfolio classic</a></li><li class="squareButton"><a href="#" data-filter=".flt_64">print</a></li><li class="squareButton"><a href="#" data-filter=".flt_10">gallery</a></li></ul>');
    }

    //Portfolio classic columns with sidebar
    if (jQuery("#style_portfolio_sb").length > 0) {
        jQuery(".isotopeFiltr", "#style_portfolio_sb").append('<ul><li class="squareButton active"><a href="#" data-filter="*">All</a></li><li class="squareButton"><a href="#" data-filter=".flt_65">clear</a></li><li class="squareButton"><a href="#" data-filter=".flt_66">exclusive</a></li><li class="squareButton"><a href="#" data-filter=".flt_7">portfolio</a></li><li class="squareButton"><a href="#" data-filter=".flt_36">Design</a></li><li class="squareButton"><a href="#" data-filter=".flt_51">portfolio classic</a></li><li class="squareButton"><a href="#" data-filter=".flt_64">print</a></li><li class="squareButton"><a href="#" data-filter=".flt_10">gallery</a></li></ul>');
    }

    //Portfolio masonry columns with sidebar
    if (jQuery("#style_portfolio_mcsb").length > 0) {
        jQuery(".isotopeFiltr", "#style_portfolio_mcsb").append('<ul><li class="squareButton active"><a href="#" data-filter="*">All</a></li><li class="squareButton"><a href="#" data-filter=".flt_36">Design</a></li><li class="squareButton"><a href="#" data-filter=".flt_7">portfolio</a></li><li class="squareButton"><a href="#" data-filter=".flt_10">gallery</a></li><li class="squareButton"><a href="#" data-filter=".flt_6">masonry</a></li><li class="squareButton"><a href="#" data-filter=".flt_64">print</a></li><li class="squareButton"><a href="#" data-filter=".flt_65">clear</a></li><li class="squareButton"><a href="#" data-filter=".flt_57">portfolio masonry</a></li></ul>');
    }

    //Portfolio grid columns
    if (jQuery("#style_portfolio_grid").length > 0) {
        jQuery(".isotopeFiltr", "#style_portfolio_grid").append('<ul><li class="squareButton active"><a href="#" data-filter="*">All</a></li><li class="squareButton"><a href="#" data-filter=".flt_65">clear</a></li><li class="squareButton"><a href="#" data-filter=".flt_36">Design</a></li><li class="squareButton"><a href="#" data-filter=".flt_66">exclusive</a></li><li class="squareButton"><a href="#" data-filter=".flt_7">portfolio</a></li><li class="squareButton"><a href="#" data-filter=".flt_63">portfolio hover</a></li><li class="squareButton"><a href="#" data-filter=".flt_64">print</a></li></ul>');
    }
};

// Select review stars
function review_star() {
    if (jQuery(".stars", "#review_form").length > 0) {
        $(".stars").find("a").on("click", function() {
            $("a.active").removeClass("active");
            $(this).addClass("active");
            return false;
        });
    }
}

//Add user popup login and sign in block
function userpopup_menu() {
    if (jQuery(".usermenu_area", "#header").length > 0) {
        var userpopUpHTML = '<div id="bejelentkezes" class="user-popUp mfp-with-anim mfp-hide">';
        userpopUpHTML += '<div class="sc_tabs">';
        userpopUpHTML += '<ul class="loginHeadTab">';
        userpopUpHTML += '<li><a href="#loginForm" class="loginFormTab icon">Bejelentkezés</a></li>';
        userpopUpHTML += '<li ><a href="#registerForm" class="registerFormTab icon" role="presentation">Regisztráció</a></li>';
        userpopUpHTML += '</ul>';

        userpopUpHTML += '<div id="loginForm" class="formItems loginFormBody">';
        userpopUpHTML += '<div class="sc_login_form itemformLeft">';
        userpopUpHTML += '<form method="post" name="login_form" class="formValid"  data-formtype="login" action="bejelentkezes">';
        userpopUpHTML += '<div class="result"></div>';
        userpopUpHTML += '<input type="hidden" name="redirect_to" value="#">';
        userpopUpHTML += '<ul class="formList">';
        userpopUpHTML += '<li class="icon formLogin"><input type="text" id="login_email" name="login_email" value="" placeholder="E-mail cím"></li>';
        userpopUpHTML += '<li class="icon formPass"><input type="password" id="login_pwd" name="login_pwd" value="" placeholder="Jelszó"></li>';

        userpopUpHTML += '<li class="remember">';
        userpopUpHTML += '<input type="checkbox" value="forever" id="rememberme" name="rememberme">';
        userpopUpHTML += '<label for="rememberme">Emlékezz rám</label>';
        userpopUpHTML += '</li>';
        userpopUpHTML += '<li><a href="bejelentkezes" class="sc_login_form_submit sendEnter enter">Bejelentkezés</a></li>';
        userpopUpHTML += '</ul>';
        userpopUpHTML += '</form>';
        userpopUpHTML += '</div>';
        userpopUpHTML += '<div class="itemformRight">';
        userpopUpHTML += '<ul class="formList">';
        //userpopUpHTML += '<li><h4 style="padding: 0px">Lépj be egy kattintással</h4></li>';
        //userpopUpHTML += '<li class="loginSoc">';
        //userpopUpHTML += '<a href="#facebook" id="facebook_btn" class="facebook_btn iconLogin fb"></a>';
        //userpopUpHTML += '<a href="#twitter" class="iconLogin tw"></a>';
        //userpopUpHTML += '<a href="#googleplus" class="iconLogin gg"></a>';
        //userpopUpHTML += '</li>';
        userpopUpHTML += '<li><br><br><a href="#" class="loginProblem">Elfelejtette a jelszavát?</a></li>';
        userpopUpHTML += '</ul>';
        userpopUpHTML += '</div>';
        userpopUpHTML += '<div class="result messageBlock"></div>';
        userpopUpHTML += '</div>';

        userpopUpHTML += '<div id="registerForm" class="sc_register_form formItems registerFormBody">';
        userpopUpHTML += '<form name="register_form" method="post" class="formValid" action="regisztracio">';
        userpopUpHTML += '<div class="result"></div>';
        userpopUpHTML += '<input type="hidden" name="redirect_to" value="#">';
        userpopUpHTML += '<div class="itemformLeft">';
        userpopUpHTML += '<ul class="formList">';
        userpopUpHTML += '<li class="icon formUser"><input type="text" id="signup_username" name="signup_username" value="" placeholder="Név"></li>';
        userpopUpHTML += '<li class="icon formLogin"><input type="text" id="signup_email" name="signup_email" value="" placeholder="E-mail"></li>';
        userpopUpHTML += '</ul>';
        userpopUpHTML += '</div>';

        userpopUpHTML += '<div class="itemformRight">';
        userpopUpHTML += '<ul class="formList">';
        userpopUpHTML += '<li class="icon formPass"><input type="password" id="signup_pwd" name="signup_pwd" value="" placeholder="Jelszó"></li>';
        userpopUpHTML += '<li class="icon formPass"><input type="password" id="signup_pwd2" name="signup_pwd2" value="" placeholder="Jelszó megerősítése"></li>';
        userpopUpHTML += '</ul>';
        userpopUpHTML += '<br>';
        userpopUpHTML += '</div>';


        userpopUpHTML += '<div class="itemformCenter">';
        userpopUpHTML += '<ul class="formList ">';
        userpopUpHTML += '<li class="i-agree">';
        userpopUpHTML += '<label for="i-agree"> <input type="checkbox" value="forever" id="i-agree" name="i-agree">A regisztrációval elfogadom az <a href="#">Adatvédelmi nyilatkozatot </a> és az oldal <a href="#">Felhasználási Feltételeit</a>.';
        userpopUpHTML += '</li>';
        userpopUpHTML += '</ul>';
        userpopUpHTML += '</div>';

        userpopUpHTML += '<div class="itemformRight">';
        userpopUpHTML += '<br>';
        userpopUpHTML += '<ul class="formList">';
        userpopUpHTML += '<li><a href="regisztracio" class="sc_register_form_submit sendEnter enter">Regisztráció</a></li>';
        userpopUpHTML += '</ul>';
        userpopUpHTML += '</div>';

        userpopUpHTML += '</form>';
        userpopUpHTML += '</div>';
        userpopUpHTML += '</div>';


        userpopUpHTML += '<button title="Bezárás (Esc)" type="button" class="mfp-close">×</button>';
        userpopUpHTML += '</div>';

        jQuery('body').append(userpopUpHTML);

    }
    initLoginButton(jQuery('body').find("#loginForm"), false);
    initFacebookLoginButton(jQuery('body').find("#bejelentkezes"));
    initSignUpButton(jQuery('body').find("#registerForm"));
}


//Add user popup admin login and sign in block
function userpopup_admin_menu() {
    if (jQuery(".usermenu_area", "#header").length > 0) {
        var adminUserpopUpHTML = '<div id="admin_bejelentkezes" class="user-popUp mfp-with-anim mfp-hide admin-login">';
        adminUserpopUpHTML += '<div class="sc_tabs">';
        adminUserpopUpHTML += '<ul class="loginHeadTab">';
        adminUserpopUpHTML += '<li><img src="pic/logo/admin_logo.png" class="logo_main" alt=""></li>';
        adminUserpopUpHTML += '</ul>';

        adminUserpopUpHTML += '<div id="adminLoginForm" class="formItems loginFormBody">';
        adminUserpopUpHTML += '<div class="sc_login_form">';
        adminUserpopUpHTML += '<form method="post" name="login_form" class="formValid"  data-formtype="login" action="bejelentkezes">';
        adminUserpopUpHTML += '<div class="result"></div>';
        adminUserpopUpHTML += '<input type="hidden" name="redirect_to" value="#">';
        adminUserpopUpHTML += '<ul class="formList">';
        adminUserpopUpHTML += '<li class="icon formLogin"><input type="text" id="login_email" name="login_email" value="" placeholder="E-mail cím"></li>';
        adminUserpopUpHTML += '<li class="icon formPass"><input type="password" id="login_pwd" name="login_pwd" value="" placeholder="Jelszó"></li>';

        adminUserpopUpHTML += '<li class="remember">';
        adminUserpopUpHTML += '<input type="checkbox" value="forever" id="rememberme" name="rememberme">';
        adminUserpopUpHTML += '<label for="rememberme">Emlékezz rám</label>';
        adminUserpopUpHTML += '</li>';
        adminUserpopUpHTML += '<li><a href="bejelentkezes" class="sc_login_form_submit sendEnter enter">Bejelentkezés</a></li>';
        adminUserpopUpHTML += '</ul>';
        adminUserpopUpHTML += '</form>';
        adminUserpopUpHTML += '</div>';

        adminUserpopUpHTML += '<div class="result messageBlock"></div>';
        adminUserpopUpHTML += '</div>';

        adminUserpopUpHTML += '</div>';

        adminUserpopUpHTML += '<button title="Bezárás (Esc)" type="button" class="mfp-close">×</button>';
        adminUserpopUpHTML += '</div>';

        jQuery('body').append(adminUserpopUpHTML);

    }
    initLoginButton(jQuery('body').find("#adminLoginForm"),true);
}

/*************************  LOGIN **************************************/

// facebook Login buttton
function initFacebookLoginButton(a){
    if (a.find(".facebook_btn:not(.inited)").length > 0) {
        a.find(".facebook_btn:not(.inited)").addClass("inited").on("click", function(f) {
            $.ajax({
                type:"POST",
                url:"facebook/url/",
                data:{
                    action:"getFacebookLoginUrl"
                },
                success:function(e){
                    console.log('response: ');
                    console.log(e);
                    window.location.href = e;
                },
                async:!1
            }).done(function(){
                //console.log(":) :) :)");
            })
            f.preventDefault();
            return false
        })
    }
}


// Login form
function initLoginButton(a, admin){
    if (a.find(".sc_login_form .sc_login_form_submit:not(.inited)").length > 0) {
        a.find(".sc_login_form .sc_login_form_submit:not(.inited)").addClass("inited").on("click", function(f) {
            var c = jQuery(this).parents("form");
            var d = c.attr("action");
            var email = document.getElementById("login_email").value;
            var pwd = document.getElementById("login_pwd").value;

            userSubmitLoginForm(c, d != undefined ? d : THEMEREX_ajax_url, THEMEREX_ajax_nonce, email, pwd, admin);
            f.preventDefault();
            return false
        })
    }
}


/* Login form error */
var THEMEREX_validateForm = null;

function userSubmitLoginForm(e, c, d, email, pwd, admin) {
    var b = false;
    var a = e.data("formtype") == "custom";
    var dataList = Array();
    if (!a) {
        b = formValidate(e, {
            error_message_show: true,
            error_message_time: 5000,
            error_message_class: "sc_infobox sc_infobox_style_error",
            error_fields_class: "error_fields_class",
            exit_after_first_error: false,
            rules: [{
                field: "login_email",
                min_length: {
                    value: 7,
                    message: THEMEREX_EMAIL_EMPTY
                },
                max_length: {
                    value: 100,
                    message: THEMEREX_EMAIL_LONG
                },
                mask: {
                    value: THEMEREX_EMAIL_MASK,
                    message: THEMEREX_EMAIL_NOT_VALID
                }
            }, {
                field: "login_pwd",
                min_length: {
                    value: 1,
                    message: THEMEREX_PWD_EMPTY
                },
                max_length: {
                    value: 2000,
                    message: THEMEREX_PWD_LONG
                }
            }]
        })
    }
    if(b){
        console.log("Login form is not valide!");
    }else{
        console.log("Login form is valide!");
    }


    if (!b && c != "#") {
        THEMEREX_validateForm = e;
        var f = {
            action: "login",
            nonce: d,
            type: a ? "custom" : "login",
            data: {
                login_email: email,
                login_pwd: pwd,
                login_admin: admin
            },
            dataType: "json"
        };
        jQuery.post(c, f, userSubmitLoginFormResponse, "text")
    }
}

function userSubmitLoginFormResponse(b) {
    console.log("Response: ");
    console.log(b);
    var c = JSON.parse(b);
    //var a = THEMEREX_validateForm.find(".result");
    var a = THEMEREX_validateForm.find(".result").toggleClass("sc_infobox_style_error", false).toggleClass("sc_infobox_style_success", false);
    if (c.error == "") {

        /*a.addClass("sc_infobox_style_success sc_infobox");
        a.html(c.message).fadeIn();
        setTimeout(function() {
            a.fadeOut();
            //THEMEREX_validateForm.get(0).reset()
        }, 2000);*/
        console.log(c.message);

        /*var currLocation = window.location.href;
        console.log('window.location.href: '+currLocation);
        var newLocation = currLocation.replace('#bejelentkezes', '');
        window.location.href = newLocation;
        console.log('window.location.href: '+newLocation);*/
        window.location.href = window.location.href.split('#')[0];
    } else {
        //a.addClass("sc_infobox_style_error").html(THEMEREX_SEND_ERROR + " " + c.error)
        a.addClass("result sc_infobox_style_error sc_infobox");
        console.log(c.message);
        a.html(c.message).fadeIn();
        setTimeout(function() {
            a.fadeOut()
        }, 5000)
    }


}

/*************************  Sign up (Regisztráció)***************************************************************/



// Sign up form
function initSignUpButton(a){
    if (a.find(".sc_register_form_submit:not(.inited)").length > 0) {
        a.find(".sc_register_form_submit:not(.inited)").addClass("inited").on("click", function(f) {
            var c = jQuery(this).parents("form");
            var d = c.attr("action");

            var name = document.getElementById("signup_username").value;
            var email = document.getElementById("signup_email").value;
            var pwd = document.getElementById("signup_pwd").value;
            var pwd2 =document.getElementById("signup_pwd2").value;


            userSubmitSignUpForm(c, d != undefined ? d : THEMEREX_ajax_url, THEMEREX_ajax_nonce, name, email, pwd, pwd2);
            f.preventDefault();
            return false
        })
    }
}


/* Sign up form error */
var THEMEREX_SignUp_validateForm = null;

function userSubmitSignUpForm(e, c, d, name, email, pwd, pwd2) {
    var b = false;
    var a = e.data("formtype") == "custom";
    var dataList = Array();
    if (!a) {
        b = formValidate(e, {
            error_message_show: true,
            error_message_time: 5000,
            error_message_class: "sc_infobox sc_infobox_style_error",
            error_fields_class: "error_fields_class",
            exit_after_first_error: false,
            rules: [{
                field: "signup_username",
                min_length: {
                    value: 3,
                    message: THEMEREX_USER_NAME_EMPTY
                },
                max_length: {
                    value: 50,
                    message: THEMEREX_USER_NAME_LONG
                }
            }, {
                field: "signup_email",
                min_length: {
                    value: 7,
                    message: THEMEREX_EMAIL_EMPTY
                },
                max_length: {
                    value: 100,
                    message: THEMEREX_EMAIL_LONG
                },
                mask: {
                    value: THEMEREX_EMAIL_MASK,
                    message: THEMEREX_EMAIL_NOT_VALID
                }
            }, {
                field: "signup_pwd",
                min_length: {
                    value: 6,
                    message: THEMEREX_PWD_EMPTY
                },
                max_length: {
                    value: 2000,
                    message: THEMEREX_PWD_LONG
                }
            }, {
                field: "signup_pwd2",
                min_length: {
                    value: 6,
                    message: THEMEREX_PWD_EMPTY
                },
                max_length: {
                    value: 2000,
                    message: THEMEREX_PWD_LONG
                },
                equal_to:{
                    value: "signup_pwd",
                    message: THEMEREX_PWDS_NOT_EQUAL
                }
            }]
        })
    }
    if(b){
        console.log(b);
        console.log("Sign up form is not valide!");
    }else{

        console.log("Sign up form is valide!");
    }


    if (!b && c != "#") {
        THEMEREX_SignUp_validateForm = e;
        var f = {
            action: "signUp",
            nonce: d,
            type: a ? "custom" : "signUp",
            data: {
                signup_username: name,
                signup_email: email,
                signup_pwd: pwd
            },
            dataType: "json"
        };
        jQuery.post(c, f, userSubmitSignUpFormResponse, "text")
    }
}

function userSubmitSignUpFormResponse(b) {
    console.log("Response: ");
    console.log(b);
    var c = JSON.parse(b);
    //var a = THEMEREX_validateForm.find(".result");
    var a = THEMEREX_SignUp_validateForm.find(".result").toggleClass("sc_infobox_style_error", false).toggleClass("sc_infobox_style_success", false);
    if (c.error == "") {
        a.addClass("sc_infobox_style_success sc_infobox");
         a.html(c.message).fadeIn();
         setTimeout(function() {
         a.fadeOut();
         //THEMEREX_validateForm.get(0).reset()
         }, 3000);
        console.log(c.message);

        /*var currLocation = window.location.href;
         console.log('window.location.href: '+currLocation);
         var newLocation = currLocation.replace('#bejelentkezes', '');
         window.location.href = newLocation;
         console.log('window.location.href: '+newLocation);*/
        window.location.href = window.location.href.split('#')[0];
    } else {
        //a.addClass("sc_infobox_style_error").html(THEMEREX_SEND_ERROR + " " + c.error)
        a.addClass("result sc_infobox_style_error sc_infobox");
        console.log(c.message);
        a.html(c.message).fadeIn();
        setTimeout(function() {
            a.fadeOut()
        }, 5000)
    }


}

/********************** Logout **********************************************************************************/


//Add user popup logout block
function userpopup_logout() {
    if (jQuery(".usermenu_area", "#header").length > 0) {
        var userpopUpLogoutHTML = '<div id="kijelentkezes" class="user-popUp mfp-with-anim mfp-hide">';
            userpopUpLogoutHTML += '<form form action="kijelentkezes" method="post">';
            userpopUpLogoutHTML += '<div  class="logout_confirm_title"><span class="fa-user"> &nbsp;&nbsp; Kijelentkezés</span></div>';
            userpopUpLogoutHTML += '<p>Valóban szeretne kijelentkezni?</p>';
            userpopUpLogoutHTML += '<div class="button_group">';
            userpopUpLogoutHTML += '<div class="sc_cancel_btn squareButton light ico"><a href="#" class="sc_cancel_btn sendEnter enter fa-arrow-left">Mégse</a></div>';
            userpopUpLogoutHTML += '<div class="sc_logout_btn squareButton light ico"><a href="#kijelentkezes" class="sc_logout_btn sendEnter enter fa-home">Igen</a></div>';

            userpopUpLogoutHTML += '</div>';
            userpopUpLogoutHTML += '</form>';
            userpopUpLogoutHTML += '<button title="Bezárás (Esc)" type="button" class="mfp-close">Bezárás</button>';
            userpopUpLogoutHTML += '</div>';

        jQuery('body').append(userpopUpLogoutHTML);


    }
    initLogoutButton(jQuery('body').find("#kijelentkezes"));
    initCancelButton(jQuery('body').find("#kijelentkezes"));
}


// Logout button
function initLogoutButton(a){
    if (a.find(".sc_logout_btn:not(.inited)").length > 0) {
        a.find(".sc_logout_btn:not(.inited)").addClass("inited").on("click", function(f) {
            /*var currLocation = window.location.href;
            console.log('curr_location: '+currLocation);
            var newLocation = currLocation.replace('#','');
            console.log('new_location: '+newLocation);
            window.location.href = newLocation;*/
            var c = jQuery(this).parents("form");
            var action = c.attr("action");

            userSubmitLogoutAction(c, action != undefined ? action : THEMEREX_ajax_url, THEMEREX_ajax_nonce);

            f.preventDefault();
            return false
        })
    }
}

function userSubmitLogoutAction(e, c , d) {
    if (c != "#") {
        var f = {
            action: "kijelentkezes",
            nonce: d,
            data: {
                logout: true,
                action: 'logout',
                controller: 'userController',
                model: 'userModel'
            },
            dataType: "json"
        };
        jQuery.post(c, f, userSubmitLogoutResponse, "text")
    }
}

function userSubmitLogoutResponse(b){

    console.log("response location: "+ window.location.hostname);
    window.location.href = window.location.href.split('#')[0];
}

// Logout, Cancel button
function initCancelButton(a){
    if (a.find(".sc_cancel_btn:not(.inited)").length > 0) {
        a.find(".sc_cancel_btn:not(.inited)").addClass("inited").on("click", function(f) {
            /*var currLocation = window.location.href;
            console.log('curr_url: '+currLocation);
            var newLocation = currLocation.replace('/#kijelentkezes','');
            console.log('new_location: '+newLocation);*/
            window.location.href = window.location.href.split('#')[0];
            f.preventDefault();
            return false
        })
    }
}


/********************** Admin DELETE Objects **********************************************************************************/


//Add user popup logout block
function userpopup_delete() {
    if (jQuery(".usermenu_area", "#header").length > 0) {
        var userpopUpDeleteHTML = '<div id="torles" class="user-popUp mfp-with-anim mfp-hide">';

        userpopUpDeleteHTML += '<form form method="post">';
        userpopUpDeleteHTML += '<div  class="delete_confirm_title"><span class="fa-remove"> &nbsp;&nbsp; Törlés</span></div>';

        userpopUpDeleteHTML += '<p>Valóban szeretne törölni a kiválasztott elemet?</p>';
        userpopUpDeleteHTML += '<div class="button_group">';
        userpopUpDeleteHTML += '<div class="sc_cancel_btn squareButton light ico"><a href="#" class="sc_cancel_btn sendEnter enter fa-arrow-left">Mégse</a></div>';
        userpopUpDeleteHTML += '<div class="sc_delete_btn squareButton light ico"><a href="#torles" class="sc_delete_btn sendEnter enter fa-remove">Igen</a></div>';

        userpopUpDeleteHTML += '</div>';
        userpopUpDeleteHTML += '</form>';
        userpopUpDeleteHTML += '<button title="Bezárás (Esc)" type="button" class="mfp-close">Bezárás</button>';
        userpopUpDeleteHTML += '<div class="result messageBlock"></div>';
        userpopUpDeleteHTML += '</div>';


        jQuery('body').append(userpopUpDeleteHTML);


    }
    //initDeleteButton(jQuery('body').find("#torles"));
    initCancelButton(jQuery('body').find("#torles"));
}

// Delete button
function initDeleteButton(action, id, url){
    var a = jQuery('body').find("#torles");
    if (a.find(".sc_delete_btn:not(.inited)").length > 0) {
        a.find(".sc_delete_btn:not(.inited)").addClass("inited").on("click", function(f) {
            var c = jQuery(this).parents("form");
            userSubmitDeleteAction(action, id, c, url != undefined ? url : THEMEREX_ajax_url, THEMEREX_ajax_nonce);

            f.preventDefault();
            return false
        })
    }
}

function userSubmitDeleteAction(action, id, e, url, d) {
    if (url != "#") {
        var f = {
            data: {
                id: id,
                action: action,
                controller: 'adminController',
                model: 'adminModel'
            },
            dataType: "json"
        };
        jQuery.post(url, f, userSubmitDeleteResponse, 'text')
    }
}

function userSubmitDeleteResponse(b){
    console.log("Response: ");
    console.log(b);
    var c = JSON.parse(b);

    var a = jQuery('#torles').find(".result").toggleClass("sc_infobox_style_error algn-center", false).toggleClass("sc_infobox_style_success algn-center", false);
    if (c.error == "") {
        a.addClass("sc_infobox_style_success sc_infobox algn-center");
        a.html(c.message).fadeIn();
        setTimeout(function() {
            a.fadeOut();
        }, 5000);
        console.log(c.message);
        window.location.href = c.url;
    } else {
        a.addClass("result sc_infobox_style_error sc_infobox algn-center");
        console.log(c.message);
        a.html(c.message).fadeIn();
        setTimeout(function() {
            a.fadeOut()
        }, 5000)
        console.log(c.message);
        window.location.href = c.url;
    }
}

// Delete, Cancel button
function initCancelButton(a){
    if (a.find(".sc_cancel_btn:not(.inited)").length > 0) {
        a.find(".sc_cancel_btn:not(.inited)").addClass("inited").on("click", function(f) {
            window.location.href = window.location.href.split('#')[0];
            f.preventDefault();
            return false
        })
    }
}


/*************************  Felhasználó, Email, Galéria, Blog és Tartalom mentése (admin felület)***************************************************************/

function initEditSubmitButton(b, action){

    var c = jQuery(b).parents("form");
    var d = c.attr("action");
    submitEditForm(c, d != undefined ? d : THEMEREX_ajax_url, THEMEREX_ajax_nonce, action);
}


/* Contact form error */
var THEMEREX_validateEditForm = null;

function submitEditForm(e, c, d, action) {
    var b = false;
    var a = e.data("formtype") == "custom";
    if (!a && action == 'updateUser') {
        b = formValidate(e, {
            error_message_show: true,
            error_message_time: 5000,
            error_message_class: "sc_infobox sc_infobox_style_error",
            error_fields_class: "error_fields_class",
            exit_after_first_error: false,
            rules: [{
                field: "name",
                min_length: {
                    value: 1,
                    message: THEMEREX_NAME_EMPTY
                },
                max_length: {
                    value: 60,
                    message: THEMEREX_NAME_LONG
                }
            }, {
                field: "email",
                min_length: {
                    value: 7,
                    message: THEMEREX_EMAIL_EMPTY
                },
                max_length: {
                    value: 60,
                    message: THEMEREX_EMAIL_LONG
                },
                mask: {
                    value: THEMEREX_EMAIL_MASK,
                    message: THEMEREX_EMAIL_NOT_VALID
                }
            }]
        })
    }

    if (!a && (action == 'updateEmail' || action == 'insertEmail')) {
        b = formValidate(e, {
            error_message_show: true,
            error_message_time: 5000,
            error_message_class: "sc_infobox sc_infobox_style_error",
            error_fields_class: "error_fields_class",
            exit_after_first_error: false,
            rules: [{
                field: "name",
                min_length: {
                    value: 1,
                    message: THEMEREX_NAME_EMPTY
                },
                max_length: {
                    value: 60,
                    message: THEMEREX_NAME_LONG
                }
            }, {
                field: "child_name",
                min_length: {
                    value: 1,
                    message: THEMEREX_NAME_EMPTY
                },
                max_length: {
                    value: 60,
                    message: THEMEREX_NAME_LONG
                }
            },{
                field: "group_name",
                min_length: {
                    value: 1,
                    message: THEMEREX_NAME_EMPTY
                },
                max_length: {
                    value: 60,
                    message: THEMEREX_NAME_LONG
                }
            },{
                field: "email",
                min_length: {
                    value: 7,
                    message: THEMEREX_EMAIL_EMPTY
                },
                max_length: {
                    value: 60,
                    message: THEMEREX_EMAIL_LONG
                },
                mask: {
                    value: THEMEREX_EMAIL_MASK,
                    message: THEMEREX_EMAIL_NOT_VALID
                }
            }]
        })
    }

    if (!a && (action == 'updateGaleryCategory' || action == 'insertGaleryCategory')) {
        b = formValidate(e, {
            error_message_show: true,
            error_message_time: 5000,
            error_message_class: "sc_infobox sc_infobox_style_error",
            error_fields_class: "error_fields_class",
            exit_after_first_error: false,
            rules: [{
                field: "title",
                min_length: {
                    value: 1,
                    message: THEMEREX_TITLE_EMPTY
                },
                max_length: {
                    value: 300,
                    message: THEMEREX_TEXT_LONG
                }
            }, {
                field: "link",
                min_length: {
                    value: 1,
                    message: THEMEREX_LINK_EMPTY
                },
                max_length: {
                    value: 300,
                    message: THEMEREX_LINK_LONG
                }
            }]
        })
    }

    if (!a && (action == 'updateGalery' || action == 'insertGalery' || action == 'updateBlog' || action == 'insertBlog' || action == 'updateContent' || action == 'insertContent')) {
        b = formValidate(e, {
            error_message_show: true,
            error_message_time: 5000,
            error_message_class: "sc_infobox sc_infobox_style_error",
            error_fields_class: "error_fields_class",
            exit_after_first_error: false,
            rules: [{
                field: "title",
                min_length: {
                    value: 1,
                    message: THEMEREX_TITLE_EMPTY
                },
                max_length: {
                    value: 300,
                    message: THEMEREX_TEXT_LONG
                }
            }, {
                field: "link",
                min_length: {
                    value: 1,
                    message: THEMEREX_LINK_EMPTY
                },
                max_length: {
                    value: 300,
                    message: THEMEREX_LINK_LONG
                }
            }, {
                field: "text",
                min_length: {
                    value: 1,
                    message: THEMEREX_TEXT_EMPTY
                },
                max_length: {
                    value: 99999999,
                    message: THEMEREX_TEXT_LONG
                }
            }]
        })
    }

    if(b){
        console.log(b);
        console.log("Form is not valide!");
    }else{

        console.log("Form is valide!");
    }

    if (!b && c != "#" && action == 'updateUser') {
        THEMEREX_validateEditForm = e;
        var name = document.getElementById("edit_form_name").value;
        var email = document.getElementById("edit_form_email").value;
        var f = {
            action: action,
            nonce: d,
            type: a ? "custom" : action,
            data: {
                name: name,
                email: email
            },
        };
        jQuery.post(c, f, submitEditFormResponse, "text")
    }

    if (!b && c != "#" && (action == 'updateEmail' || action == 'insertEmail')) {
        THEMEREX_validateEditForm = e;
        var name = document.getElementById("edit_form_name").value;
        var child_name = document.getElementById("edit_form_child_name").value;
        var group_name = document.getElementById("edit_form_group_name").value;
        var email = document.getElementById("edit_form_email").value;
        var f = {
            action: action,
            nonce: d,
            type: a ? "custom" : action,
            data: {
                name: name,
                child_name: child_name,
                group_name: group_name,
                email: email
            },
        };
        jQuery.post(c, f, submitEditFormResponse, "text")
    }

    if (!b && c != "#" && (action == 'updateGaleryCategory' || action == 'insertGaleryCategory')) {
        THEMEREX_validateEditForm = e;

        var title = document.getElementById("edit_form_title").value;
        var link = document.getElementById("edit_form_link").value;
        var f = {
            action: action,
            nonce: d,
            type: a ? "custom" : action,
            data: {
                title: title,
                link: link
            },
        };
        jQuery.post(c, f, submitEditFormResponse, "text")
    }

    if (!b && c != "#" && (action == 'updateGalery' || action == 'insertGalery')) {
        THEMEREX_validateEditForm = e;

        var category_id = document.getElementById("edit_form_category").value;
        var title = document.getElementById("edit_form_title").value;
        var link = document.getElementById("edit_form_link").value;
        var text = CKEDITOR.instances['edit_form_text'].getData();
        var image_count = document.getElementById("file-upload").files.length;
        var xhr = new XMLHttpRequest();

        xhr.open('POST', c, true);
        //xhr.setRequestHeader("Content-type", "application/json;charset=UTF-8");

        var data = new FormData();
        data.append("category_id", category_id);
        data.append("title", title);
        data.append("link", link);
        data.append("text", text);
        data.append("image_count", image_count);

        xhr.onload = function () {
            if (this.status == 200) {
                submitEditFormResponse(this.response)
            }
        };
        xhr.send(data);
    }

    if (!b && c != "#" && (action == 'updateBlog' || action == 'insertBlog')) {
        THEMEREX_validateEditForm = e;

        var galery_id = document.getElementById("edit_form_galery").value;
        var title = document.getElementById("edit_form_title").value;
        var link = document.getElementById("edit_form_link").value;
        var text = CKEDITOR.instances['edit_form_text'].getData();

        var f = {
            action: action,
            nonce: d,
            type: a ? "custom" : action,
            data: {
                title: title,
                link: link,
                text: text,
                galery_id : galery_id,

            },
        };
        jQuery.post(c, f, submitEditFormResponse, "text")
    }

    if (!b && c != "#" && (action == 'updateContent' || action == 'insertContent')) {
        THEMEREX_validateEditForm = e;

        var title = document.getElementById("edit_form_title").value;
        var link = document.getElementById("edit_form_link").value;
        var text = CKEDITOR.instances['edit_form_text'].getData();
        console.log('text: ');
        console.log(text);

        var f = {
            action: action,
            nonce: d,
            type: a ? "custom" : action,
            data: {
                title: title,
                link: link,
                text: text
            },
        };
        jQuery.post(c, f, submitEditFormResponse, "text")
    }

}

function submitEditFormResponse(b) {
    console.log("Response: ");
    console.log(b);
    var c = JSON.parse(b);

    var a = jQuery('#editForm').find(".result").toggleClass("sc_infobox_style_error algn-center", false).toggleClass("sc_infobox_style_success algn-center", false);
    if (c.error == "") {
        a.addClass("sc_infobox_style_success sc_infobox");
        a.html(c.message).fadeIn();
        setTimeout(function() {
            a.fadeOut();
        }, 5000);
        console.log(c.message);
        if (c.url != ""){
            window.location.href = c.url;
        }
        //window.location.href = c.url;
    } else {
        a.addClass("result sc_infobox_style_error sc_infobox");
        console.log(c.message);
        a.html(c.message).fadeIn();
        setTimeout(function() {
            a.fadeOut()
        }, 5000);
        console.log(c.message);
        if (c.url != ""){
            window.location.href = c.url;
        }
    }


}

/***********************************************************************************************************************/
/*
function ready() {

    // Show system message
    if (THEMEREX_systemMessage.message) {
        if (THEMEREX_systemMessage.status == "success") {
            themerex_message_success(THEMEREX_systemMessage.message, THEMEREX_systemMessage.header)
        } else {
            if (THEMEREX_systemMessage.status == "info") {
                themerex_message_info(THEMEREX_systemMessage.message, THEMEREX_systemMessage.header)
            } else {
                if (THEMEREX_systemMessage.status == "error" || THEMEREX_systemMessage.status == "warning") {
                    themerex_message_warning(THEMEREX_systemMessage.message, THEMEREX_systemMessage.header)
                }
            }
        }
    }
}
*/


/*
function saveValami(e){
    var t=document.getElementById("editable_hir_title").textContent,
        p=document.getElementById("editable_hir_leiras").textContent,
        o=document.getElementById("editable_tartalom").innerHTML,
        n=document.getElementById("uj_hir_image").src;
    console.clear(),
        $.ajax({
            type:"POST",
            url:"library/modification_lib/",
            data:{action:"insertTrashtalk",
                user_id:e,
                cim:t,
                leiras:p,
                imgBase64:n,
                tartalom:o
            },
            success:function(e){
                location.href = e;
            },
            async:!1
        }).done(function(){
            console.log("Új TrashTalk hozzáadása sikeres volt! :) :) :)")
        })
}*/


