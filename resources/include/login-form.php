<?php

//////////////////////////
//Specify default values//
//////////////////////////

$data = $_POST['data'];


//Message if 'name' field not specified
$email_not_specified = 'Please type a valid name';

//Message if 'message' field not specified
$message_not_specified = 'Please type a vaild message';

//Message if e-mail sent successfully
$login_success = '{"error":"", "message":"Thanks, your are successfully logged!"}';



///////////////////////////
//Login Form Processing//
///////////////////////////
$errors = array();
if(isset($data['login_email']) and isset($data['login_pwd'])) {
	if(!empty($data['login_email']))
		$login_email = stripslashes(strip_tags(trim($data['login_email'])));
	
	if(!empty($data['login_pwd']))
		$login_pwd = stripslashes(strip_tags(trim($data['login_pwd'])));


	//Message if no login email was specified
	if(empty($login_email)) {
		$errors[] = $email_not_specified;
	}

	//Message if no login password was specified
	if(empty($login_pwd)) {
		$errors[] = $message_not_specified;
	}

	//Login process
	if(empty($errors)) {
		$result = login($login_email, $login_pwd);
		if ($result) {
			echo $login_success;
		} else {
			$errors[] = $server_not_configured;
			echo implode('<br>', $errors );
		}
	} else {
		echo implode('<br>', $errors );
	}
} else {
	echo '"login_email" and "login_pwd" variables were not received by server. Please check "login_email", "login_pwd" attributes for your input fields';
}

function login($email, $pwd){

	include_once "../../modul/user/model/userModel.php";
	include_once "../../objects/User.php";
	$userModel = new userModel();
	$userModel->login($email, $pwd);
	$user = $userModel->user;
	if(null != $user){
		echo $user->toJson();
		return true;
	}else{
		return false;
	}
}
?>