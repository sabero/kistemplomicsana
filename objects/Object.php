<?php

/**
 * Created by PhpStorm.
 * User: Ouechtati Saber
 * Date: 2015.10.09.
 * Time: 22:37
 */


class Object
{
    protected $table;

    protected $id;
    protected $assocValues;
    protected $values;
    protected $active = 1;
    protected $deleted = 0;
    protected $lastId;
    protected $sql;

    /**
     * Object constructor.
     */
    public function __construct($table)
    {
        $this->table = $table;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getAssocValues()
    {
        return $this->assocValues;
    }

    /**
     * @param mixed $assocValues
     */
    public function setAssocValues($assocValues)
    {
        $this->assocValues = $assocValues;
    }

    /**
     * @return mixed
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * @param mixed $values
     */
    public function setValues($values)
    {
        $this->values = $values;
    }

    /**
     * @return int
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param int $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return int
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param int $deleted
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }

    /**
     * @return mixed
     */
    public function getLastId()
    {
        return $this->lastId;
    }

    /**
     * @param mixed $lastId
     */
    public function setLastId($lastId)
    {
        $this->lastId = $lastId;
    }

    /**
     * @return mixed
     */
    public function getSql()
    {
        return $this->sql;
    }

    /**
     * @param mixed $sql
     */
    public function setSql($sql)
    {
        $this->sql = $sql;
    }


    /**
     * @return mixed JSON string
     */
    public  function toJson(){
        return preg_replace("/\\\\u([a-f0-9]{4})/e", "iconv('UCS-4LE','UTF-8',pack('V', hexdec('U$1')))",json_encode($this));
    }

    /**
     * @param $pdo
     * @param array $values
     * @return mixed
     */
    public function insert($pdo)
    {
        $values = $this->getAssocValues();
        foreach ($values as $field => $v)
            $ins[] = ':' . $field;

        $ins = implode(',', $ins);
        $fields = implode(',', array_keys($values));
        $sql = "INSERT INTO $this->table ($fields) VALUES ($ins)";

        $sth = $pdo->prepare($sql);
        foreach ($values as $f => $v)
        {
            $sth->bindValue(':' . $f, $v);
        }
        $sth->execute();
        $this->setLastId($pdo->lastInsertId());

        //ChromePhp::log('lastInsertId: '.$pdo->lastInsertId());

        return $this->getLastId();
    }

    /**
     * @param $pdo
     * @param array $values
     * @return mixed
     */
    public function update($pdo)
    {
        ChromePhp::log($this->getValues());
        $sth = $pdo->prepare($this->getSql());
        $sth->execute($this->getValues());
        return $sth->rowCount();
    }
}