<?php

/**
 * Created by PhpStorm.
 * User: Ouechtati Saber
 * Date: 2015.10.09.
 * Time: 22:37
 */

include_once 'Object.php';
include_once 'DateToHunDatum.php';

class Email_list extends Object
{

    private $name;
    private $child_name;
    private $group_name;
    private $email;

    /**
     * @constructor
     */
    function __construct()
    {
        parent::__construct(strtolower(get_class()));
    }



    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getChildName()
    {
        return $this->child_name;
    }

    /**
     * @param mixed $child_name
     */
    public function setChildName($child_name)
    {
        $this->child_name = $child_name;
    }

    /**
     * @return mixed
     */
    public function getGroupName()
    {
        return $this->group_name;
    }

    /**
     * @param mixed $group_name
     */
    public function setGroupName($group_name)
    {
        $this->group_name = $group_name;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }



    public function getAssocValues(){
        return array(
            'name' => $this->getName(),
            'child_name' => $this->getChildName(),
            'group_name' => $this->getGroupName(),
            'email' => $this->getEmail(),
            'active' => $this->getActive(),
            'deleted' => $this->getDeleted()
        );
    }

    public function getValues(){
        return array(
            $this->getName(),
            $this->getChildName(),
            $this->getGroupName(),
            $this->getEmail(),
            $this->getActive(),
            $this->getDeleted(),
            $this->id
        );
    }

    /**
     * @return string
     */
    public function getSql()
    {
        return "UPDATE email_list SET
                    name=?,
                    child_name=?,
                    group_name=?,
                    email=?,
                    active=?,
                    deleted=?
                    WHERE id=?
              ";
    }

}