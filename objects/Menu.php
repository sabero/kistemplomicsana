<?php

/**
 * Created by PhpStorm.
 * User: Ouechtati Saber
 * Date: 2015.10.09.
 * Time: 16:45
 */
class Menu
{
    public $menu_id;
    public $language_id;
    public $level;
    public $left_side;
    public $right_side;
    public $menu_name;
    public $menu_link;
    public $rights_group_id;
    public $menu_active;
    public $menu_deleted;

    /**
     * @return mixed
     */
    public function getMenuId()
    {
        return $this->menu_id;
    }

    /**
     * @param mixed $menu_id
     */
    public function setMenuId($menu_id)
    {
        $this->menu_id = $menu_id;
    }

    /**
     * @return mixed
     */
    public function getLanguageId()
    {
        return $this->language_id;
    }

    /**
     * @param mixed $language_id
     */
    public function setLanguageId($language_id)
    {
        $this->language_id = $language_id;
    }

    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param mixed $level
     */
    public function setLevel($level)
    {
        $this->level = $level;
    }

    /**
     * @return mixed
     */
    public function getLeftSide()
    {
        return $this->left_side;
    }

    /**
     * @param mixed $left_side
     */
    public function setLeftSide($left_side)
    {
        $this->left_side = $left_side;
    }

    /**
     * @return mixed
     */
    public function getRightSide()
    {
        return $this->right_side;
    }

    /**
     * @param mixed $right_side
     */
    public function setRightSide($right_side)
    {
        $this->right_side = $right_side;
    }

    /**
     * @return mixed
     */
    public function getMenuName()
    {
        return $this->menu_name;
    }

    /**
     * @param mixed $menu_name
     */
    public function setMenuName($menu_name)
    {
        $this->menu_name = $menu_name;
    }

    /**
     * @return mixed
     */
    public function getMenuLink()
    {
        return $this->menu_link;
    }

    /**
     * @param mixed $menu_link
     */
    public function setMenuLink($menu_link)
    {
        $this->menu_link = $menu_link;
    }

    /**
     * @return mixed
     */
    public function getRightsGroupId()
    {
        return $this->rights_group_id;
    }

    /**
     * @param mixed $rights_group_id
     */
    public function setRightsGroupId($rights_group_id)
    {
        $this->rights_group_id = $rights_group_id;
    }

    /**
     * @return mixed
     */
    public function getMenuActive()
    {
        return $this->menu_active;
    }

    /**
     * @param mixed $menu_active
     */
    public function setMenuActive($menu_active)
    {
        $this->menu_active = $menu_active;
    }

    /**
     * @return mixed
     */
    public function getMenuDeleted()
    {
        return $this->menu_deleted;
    }

    /**
     * @param mixed $menu_deleted
     */
    public function setMenuDeleted($menu_deleted)
    {
        $this->menu_deleted = $menu_deleted;
    }



    /**
     * @return mixed JSON string
     */
    public  function toJson(){
        return preg_replace("/\\\\u([a-f0-9]{4})/e", "iconv('UCS-4LE','UTF-8',pack('V', hexdec('U$1')))",json_encode($this));
    }

}