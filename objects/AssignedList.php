<?php

 /**
  * Created by PhpStorm.
  * User: Ouechtati Saber
  * Date: 2015.10.09.
  * Time: 22:37
  */


class AssignedList
{

    private $list;

    /**
     * @constructor
     */
    function __construct()
    {
        $this->list = array();
    }

    public function assign($key, $value){
        $this->list[$key] = $value;
    }

    /**
     * @return array
     */
    public function getList()
    {
        return $this->list;
    }

    /**
     * @param array $list
     */
    public function setList($list)
    {
        $this->list = $list;
    }

}