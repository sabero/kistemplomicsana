<?php

/**
 * Created by PhpStorm.
 * User: Ouechtati Saber
 * Date: 2015.10.09.
 * Time: 22:37
 */


class DateToHunDatum
{

    private $date;
    private $datum;    
    private $ev;
    private $ho;
    private $nap;
    private $ora;
    private $perc;
    private $masodperc;

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getDatum()
    {
        return $this->datum;
    }

    /**
     * @param string $datum
     */
    public function setDatum($datum)
    {
        $this->datum = $datum;
    }

    /**
     * @return mixed
     */
    public function getEv()
    {
        return $this->ev;
    }

    /**
     * @param mixed $ev
     */
    public function setEv($ev)
    {
        $this->ev = $ev;
    }

    /**
     * @return mixed
     */
    public function getHo()
    {
        return $this->ho;
    }

    /**
     * @param mixed $ho
     */
    public function setHo($ho)
    {
        $this->ho = $ho;
    }

    /**
     * @return mixed
     */
    public function getNap()
    {
        return $this->nap;
    }

    /**
     * @param mixed $nap
     */
    public function setNap($nap)
    {
        $this->nap = $nap;
    }

    /**
     * @return mixed
     */
    public function getOra()
    {
        return $this->ora;
    }

    /**
     * @param mixed $ora
     */
    public function setOra($ora)
    {
        $this->ora = $ora;
    }

    /**
     * @return mixed
     */
    public function getPerc()
    {
        return $this->perc;
    }

    /**
     * @param mixed $perc
     */
    public function setPerc($perc)
    {
        $this->perc = $perc;
    }

    /**
     * @return mixed
     */
    public function getMasodperc()
    {
        return $this->masodperc;
    }

    /**
     * @param mixed $masodperc
     */
    public function setMasodperc($masodperc)
    {
        $this->masodperc = $masodperc;
    }



    function __construct($date)
    {
        $this->setDate($date);

        $timeStamp =strtotime($date);
        $this->setEv(date('Y',$timeStamp));
        $this->setHo(date('m',$timeStamp));
        $this->setNap(date('d',$timeStamp));
        $this->setOra(date("H",$timeStamp));
        $this->setPerc(date("i",$timeStamp));
        $this->setMasodperc(date("s",$timeStamp));

        $this->setDatum($this->datum_str().' ('.$this->ido().')');
    }

    public function datum_str()
    {
        $ma=date("Y-m-d");
        $timeStamp_1 =strtotime($ma);
        $year_1 =date('Y',$timeStamp_1);
        $month_1 =date('m',$timeStamp_1);
        $day_1 =date('d',$timeStamp_1);

        if(($year_1==$this->getEv()) && ($month_1==$this->getHo()) && ($day_1==$this->getNap())) { $datum_Str =" Ma ";}
        else if(($year_1==$this->getEv()) && ($month_1==$this->getHo()) && (($day_1-1)==$this->getNap())) {$datum_Str =" Tegnap ";}
        else if(($year_1==$this->getEv()) && ($month_1==$this->getHo()) && (($day_1-2)==$this->getNap())) {$datum_Str =" Tegnapelött ";}
        else{ $datum_Str =$this->getEv().".".$this->getHo().".".$this->getNap().".";}

        return $datum_Str;
    }

    public function ido()
    {
        $datum_Str =$this->getOra().":".$this->getPerc();
        return $datum_Str;
    }
}