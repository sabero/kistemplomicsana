<?php

/**
 * Created by PhpStorm.
 * User: Ouechtati Saber
 * Date: 2015.10.09.
 * Time: 22:45
 */

include_once 'Object.php';
include_once 'DateToHunDatum.php';

class Blog extends Object
{

    private $title;
    private $text;
    private $link;
    private $create_date;
    private $galery_id;
    private $galery_title;
    private $start_date;
    private $end_date;

    private $views;
    private $comments;
    private $likes;


    /**
     * Galery constructor.
     */
    public function __construct()
    {
        $this->table = strtolower(get_class());
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $description
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param mixed $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * @return mixed
     */
    public function getCreateDate()
    {
        return $this->create_date;
    }

    /**
     * @return mixed
     */
    public function getHunCreateDate()
    {
        $converter = new DateToHunDatum($this->create_date);
        return $converter->getDatum();
    }

    /**
     * @param mixed $create_date
     */
    public function setCreateDate($create_date)
    {
        $this->create_date = $create_date;
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->start_date;
    }

    /**
     * @return mixed
     */
    public function getHunStartDate()
    {
        $converter = new DateToHunDatum($this->start_date);
        return $converter->getDatum();
    }

    /**
     * @param mixed $start_date
     */
    public function setStartDate($start_date)
    {
        $this->start_date = $start_date;
    }

    /**
     * @return mixed
     */
    public function getEndDate()
    {
        return $this->end_date;
    }

    /**
     * @return mixed
     */
    public function getHunEndDate()
    {
        $converter = new DateToHunDatum($this->end_date);
        return $converter->getDatum();
    }

    /**
     * @return mixed
     */
    public function getMonthAndDay()
    {
        $converter = new DateToHunDatum($this->create_date);
        return $converter->getHo().'.'.$converter->getNap();
    }

    /**
     * @return mixed
     */
    public function getYear()
    {
        $converter = new DateToHunDatum($this->create_date);
        return $converter->getEv();
    }

    /**
     * @param mixed $end_date
     */
    public function setEndDate($end_date)
    {
        $this->end_date = $end_date;
    }

    /**
     * @return mixed
     */
    public function getGaleryId()
    {
        return $this->galery_id;
    }

    /**
     * @param mixed $galery_id
     */
    public function setGaleryId($galery_id)
    {
        $this->galery_id = $galery_id;
    }

    /**
     * @return mixed
     */
    public function getGaleryTitle()
    {
        return $this->galery_title;
    }

    /**
     * @param mixed $galery_title
     */
    public function setGaleryTitle($galery_title)
    {
        $this->galery_title = $galery_title;
    }

    /**
     * @return mixed
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * @param mixed $views
     */
    public function setViews($views)
    {
        $this->views = $views;
    }

    /**
     * @return mixed
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param mixed $comments
     */
    public function setComments($comments)
    {
        $this->comments = $comments;
    }

    /**
     * @return mixed
     */
    public function getLikes()
    {
        return $this->likes;
    }

    /**
     * @param mixed $likes
     */
    public function setLikes($likes)
    {
        $this->likes = $likes;
    }





    /**
     * @return array
     */
    public function getAssocValues()
    {
        return array(
            'title' => $this->getTitle(),
            'text' => $this->getText(),
            'link' => $this->getLink(),
            'create_date' => $this->getCreateDate(),
            'galery_id' => $this->getGaleryId(),
            'galery_title' => $this->getGaleryTitle(),
            'views' => $this->getViews(),
            'comments' => $this->getComments(),
            'likes' => $this->getLikes(),
            'active' => $this->getActive(),
            'deleted' => $this->getDeleted()
        );
    }

    /**
     * @return array
     */
    public function getValues()
    {
        return array(
            $this->getTitle(),
            $this->getText(),
            $this->getLink(),
            $this->getCreateDate(),
            $this->getGaleryId(),
            $this->getGaleryTitle(),
            $this->getViews(),
            $this->getComments(),
            $this->getLikes(),
            $this->getActive(),
            $this->getDeleted(),
            $this->getId()
        );
    }

    /**
     * @return string
     */
    public function getSql()
    {
        return "UPDATE blog SET
                    title=?,
                    text=?,
                    link=?,
                    create_date=?,
                    galery_id=?,
                    galery_title=?,
                    views=?,
                    comments=?,
                    likes=?,
                    active=?,
                    deleted=?
                    WHERE id=?
              ";
    }

    public function delete($pdo){

        $sql ="DELETE FROM blog WHERE id=?";
        $sth = $pdo->prepare($sql);
        return $sth->execute(array($this->getId()));
    }

}

?>