<?php

/**
 * Created by PhpStorm.
 * User: Ouechtati Saber
 * Date: 2015.10.09.
 * Time: 22:45
 */

include_once 'Object.php';
include_once 'DateToHunDatum.php';

class Content extends Object
{

    private $title;
    private $text;
    private $link;
    private $create_date;


    /**
     * Galery constructor.
     */
    public function __construct()
    {
        $this->table = strtolower(get_class());
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $description
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param mixed $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * @return mixed
     */
    public function getCreateDate()
    {
        return $this->create_date;
    }

    /**
     * @return mixed
     */
    public function getHunCreateDate()
    {
        $converter = new DateToHunDatum($this->create_date);
        return $converter->getDatum();
    }

    /**
     * @param mixed $create_date
     */
    public function setCreateDate($create_date)
    {
        $this->create_date = $create_date;
    }

    /**
     * @return array
     */
    public function getAssocValues()
    {
        return array(
            'title' => $this->getTitle(),
            'text' => $this->getText(),
            'link' => $this->getLink(),
            'create_date' => $this->getCreateDate(),
            'active' => $this->getActive(),
            'deleted' => $this->getDeleted()
        );
    }

    /**
     * @return array
     */
    public function getValues()
    {
        return array(
            $this->getTitle(),
            $this->getText(),
            $this->getLink(),
            $this->getCreateDate(),
            $this->getActive(),
            $this->getDeleted(),
            $this->getId()
        );
    }

    /**
     * @return string
     */
    public function getSql()
    {
        return "UPDATE content SET
                    title=?,
                    text=?,
                    link=?,
                    create_date=?,
                    active=?,
                    deleted=?
                    WHERE id=?
              ";
    }

}

?>