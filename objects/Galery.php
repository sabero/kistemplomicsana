<?php

/**
 * Created by PhpStorm.
 * User: Ouechtati Saber
 * Date: 2015.10.09.
 * Time: 22:45
 */

include_once 'Object.php';
include_once 'Galery_category.php';
include_once 'DateToHunDatum.php';

class Galery extends Object
{

    private $title;
    private $text;
    private $link;
    private $create_date;
    private $category_id;
    private $category_title;


    /**
     * Galery constructor.
     */
    public function __construct()
    {
        $this->table = strtolower(get_class());
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $description
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param mixed $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * @return mixed
     */
    public function getCreateDate()
    {
        return $this->create_date;
    }

    /**
     * @return mixed
     */
    public function getHunCreateDate()
    {
        $converter = new DateToHunDatum($this->create_date);
        return $converter->getDatum();
    }

    /**
     * @param mixed $create_date
     */
    public function setCreateDate($create_date)
    {
        $this->create_date = $create_date;
    }

    /**
     * @return mixed
     */
    public function getCategoryId()
    {
        return $this->category_id;
    }

    /**
     * @param mixed $category
     */
    public function setCategoryId($category_id)
    {
        $this->category_id = $category_id;
    }

    /**
     * @return mixed
     */
    public function getCategoryTitle()
    {
        return $this->category_title;
    }

    /**
     * @param mixed $category_title
     */
    public function setCategoryTitle($category_title)
    {
        $this->category_title = $category_title;
    }


    /**
     * @return array
     */
    public function getAssocValues()
    {
        return array(
            'title' => $this->getTitle(),
            'text' => $this->getText(),
            'link' => $this->getLink(),
            'create_date' => $this->getCreateDate(),
            'category_id' => $this->getCategoryId(),
            'category_title' => $this->getCategoryTitle(),
            'active' => $this->getActive(),
            'deleted' => $this->getDeleted()
        );
    }

    /**
     * @return array
     */
    public function getValues()
    {
        return array(
            $this->getTitle(),
            $this->getText(),
            $this->getLink(),
            $this->getCreateDate(),
            $this->getCategoryId(),
            $this->getCategoryTitle(),
            $this->getActive(),
            $this->getDeleted(),
            $this->getId()
        );
    }

    /**
     * @return string
     */
    public function getSql()
    {
        return "UPDATE galery SET
                    title=?,
                    text=?,
                    link=?,
                    create_date=?,
                    category_id=?,
                    category_title=?,
                    active=?,
                    deleted=?
                    WHERE id=?
              ";
    }

    public function delete($pdo){

        $sql ="DELETE FROM galery WHERE id=?";
        $sth = $pdo->prepare($sql);
        if($this->deleteGaleryPhotos($pdo)){
            return $sth->execute(array($this->getId()));
        }
        return false;
    }

    public function deleteGaleryPhotos($pdo){

        $sql ="DELETE FROM photo WHERE galery_id=?";
        $sth = $pdo->prepare($sql);
        return $sth->execute(array($this->getId()));
    }

}

?>