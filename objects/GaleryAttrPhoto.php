<?php

/**
 * Created by PhpStorm.
 * User: Ouechtati Saber
 * Date: 2015.10.09.
 * Time: 22:45
 */

include_once 'Object.php';

class GaleryAttrPhoto extends Object
{

    private $galery_id;
    private $photo_id;


    public function __construct()
    {
        $this->table = strtolower(get_class());
    }

    /**
     * @return mixed
     */
    public function getGaleryId()
    {
        return $this->galery_id;
    }

    /**
     * @param mixed $galery_id
     */
    public function setGaleryId($galery_id)
    {
        $this->galery_id = $galery_id;
    }

    /**
     * @return mixed
     */
    public function getPhotoId()
    {
        return $this->photo_id;
    }

    /**
     * @param mixed $photo_id
     */
    public function setPhotoId($photo_id)
    {
        $this->photo_id = $photo_id;
    }



    /**
     * @return array
     */
    public function getAssocValues()
    {
        return array(
            'galery_id' => $this->getGaleryId(),
            'photo_id' => $this->getPhotoId(),
            'active' => $this->getActive(),
            'deleted' => $this->getDeleted()
        );
    }

    /**
     * @return array
     */
    public function getValues()
    {
        return array(
            $this->getGaleryId(),
            $this->getPhotoId(),
            $this->getActive(),
            $this->getDeleted(),
            $this->getId()
        );
    }

    /**
     * @return string
     */
    public function getSql()
    {
        return "UPDATE user SET
                    galery_id=?,
                    photo_id=?,
                    active=?,
                    deleted=?
                    WHERE id=?
              ";
    }

}

?>