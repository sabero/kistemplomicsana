<?php

 /**
 * Created by PhpStorm.
 * User: Ouechtati Saber
 * Date: 2015.10.09.
 * Time: 22:37
 */

include_once 'Object.php';
include_once 'DateToHunDatum.php';

class User extends Object
{

    private $name;
    private $password;
    private $email;
    private $profile_photo = 'pic/user/user_image.png';
    private $type = '2';
    private $another_id;
    private $reg_date;
    private $confirmed = 1;
    private $confirmation_date;
    private $last_login;
    private $szum_login = 1;

    /**
     * User constructor.
     */
    public function __construct()
    {
        parent::__construct(strtolower(get_class()));
    }


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getProfilePhoto()
    {
        return $this->profile_photo;
    }

    /**
     * @param mixed $profile_photo
     */
    public function setProfilePhoto($profile_photo)
    {
        $this->profile_photo = $profile_photo;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getAnotherId()
    {
        return $this->another_id;
    }

    /**
     * @param mixed $another_id
     */
    public function setAnotherId($another_id)
    {
        $this->another_id = $another_id;
    }

    /**
     * @return mixed
     */
    public function getRegDate()
    {
        return $this->reg_date;
    }

    /**
     * @return mixed
     */
    public function getHunRegDate()
    {
        $converter = new DateToHunDatum($this->reg_date);
        return $converter->getDatum();
    }

    /**
     * @param mixed $reg_date
     */
    public function setRegDate($reg_date)
    {
        $this->reg_date = $reg_date;
    }

    /**
     * @return mixed
     */
    public function getConfirmed()
    {
        return $this->confirmed;
    }

    /**
     * @param mixed $confirmed
     */
    public function setConfirmed($confirmed)
    {
        $this->confirmed = $confirmed;
    }

    /**
     * @return mixed
     */
    public function getConfirmationDate()
    {
        return $this->confirmation_date;
    }

    /**
     * @return mixed
     */
    public function getHunConfirmationDate()
    {
        $converter = new DateToHunDatum($this->confirmation_date);
        return $converter->getDatum();
    }

    /**
     * @param mixed $confirmation_date
     */
    public function setConfirmationDate($confirmation_date)
    {
        $this->confirmation_date = $confirmation_date;
    }

    /**
     * @return mixed
     */
    public function getLastLogin()
    {
        return $this->last_login;
    }

    /**
     * @return mixed
     */
    public function getHunLastLogin()
    {
        $converter = new DateToHunDatum($this->last_login);
        return $converter->getDatum();
    }

    /**
     * @param mixed $last_login
     */
    public function setLastLogin($last_login)
    {
        $this->last_login = $last_login;
    }

    /**
     * @return mixed
     */
    public function getSzumLogin()
    {
        return $this->szum_login;
    }

    /**
     * @param mixed $szum_login
     */
    public function setSzumLogin($szum_login)
    {
        $this->szum_login = $szum_login;
    }

    /**
     * @return array
     */
    public function getAssocValues()
    {
        return array(
            'name' => $this->getName(),
            'password' => $this->getPassword(),
            'email' => $this->getEmail(),
            'profile_photo' => $this->getProfilePhoto(),
            'type' => $this->getType(),
            'another_id' => $this->getAnotherId(),
            'reg_date' => $this->getRegDate(),
            'confirmed' => $this->getConfirmed(),
            'confirmation_date' => $this->getConfirmationDate(),
            'last_login' => $this->getLastLogin(),
            'szum_login' => $this->getSzumLogin(),
            'active' => $this->getActive(),
            'deleted' => $this->getDeleted()
        );
    }

    /**
     * @return array
     */
    public function getValues()
    {
        return array(
            $this->getName(),
            $this->getPassword(),
            $this->getEmail(),
            $this->getProfilePhoto(),
            $this->getType(),
            $this->getAnotherId(),
            $this->getRegDate(),
            $this->getConfirmed(),
            $this->getConfirmationDate(),
            $this->getLastLogin(),
            $this->getSzumLogin(),
            $this->getActive(),
            $this->getDeleted(),
            $this->getId()
        );
    }

    /**
     * @return string
     */
    public function getSql()
    {
        return "UPDATE user SET
                    name=?,
                    password=?,
                    email=?,
                    profile_photo=?,
                    type=?,
                    another_id=?,
                    reg_date=?,
                    confirmed=?,
                    confirmation_date=?,
                    last_login=?,
                    szum_login=?,
                    active=?,
                    deleted=?
                    WHERE id=?
              ";
    }

}

?>