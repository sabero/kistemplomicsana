<?php

/**
 * Created by PhpStorm.
 * User: Ouechtati Saber
 * Date: 2015.10.09.
 * Time: 22:45
 */

include_once 'Object.php';
include_once 'DateToHunDatum.php';

class Post extends Object
{

    private $user_id;
    private $blog_id;
    private $text;
    private $create_date;


    /**
     * Post constructor.
     */
    public function __construct()
    {
        parent::__construct(strtolower(get_class()));
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $title
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return mixed
     */
    public function getBlogId()
    {
        return $this->blog_id;
    }

    /**
     * @param mixed $blog_id
     */
    public function setBlogId($blog_id)
    {
        $this->blog_id = $blog_id;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $description
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param mixed $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * @return mixed
     */
    public function getCreateDate()
    {
        return $this->create_date;
    }

    /**
     * @return mixed
     */
    public function getHunCreateDate()
    {
        $converter = new DateToHunDatum($this->create_date);
        return $converter->getDatum();
    }

    /**
     * @param mixed $create_date
     */
    public function setCreateDate($create_date)
    {
        $this->create_date = $create_date;
    }

    /**
     * @return mixed
     */
    public function getCategoryId()
    {
        return $this->category_id;
    }

    /**
     * @param mixed $category
     */
    public function setCategoryId($category_id)
    {
        $this->category_id = $category_id;
    }

    /**
     * @return array
     */
    public function getAssocValues()
    {
        return array(
            'user_id' => $this->getUserId(),
            'blog_id' => $this->getBlogId(),
            'text' => $this->getText(),
            'link' => $this->getLink(),
            'create_date' => $this->getCreateDate(),
            'category_id' => $this->getCategoryId(),
            'active' => $this->getActive(),
            'deleted' => $this->getDeleted()
        );
    }

    /**
     * @return array
     */
    public function getValues()
    {
        return array(
            $this->getUserId(),
            $this->getBlogId(),
            $this->getText(),
            $this->getLink(),
            $this->getCreateDate(),
            $this->getCategoryId(),
            $this->getActive(),
            $this->getDeleted(),
            $this->getId()
        );
    }

    /**
     * @return string
     */
    public function getSql()
    {
        return "UPDATE post SET
                    user_id=?,
                    blog_id=?,
                    text=?,
                    link=?,
                    create_date=?,
                    category_id=?,
                    active=?,
                    deleted=?
                    WHERE id=?
              ";
    }

}

?>