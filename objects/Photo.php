<?php

/**
 * Created by PhpStorm.
 * User: Ouechtati Saber
 * Date: 2015.10.09.
 * Time: 22:45
 */

include_once 'Object.php';
include_once 'DateToHunDatum.php';

class Photo extends Object
{

    private $title;
    private $src;
    private $create_date;
    private $galery_id;

    private $name;
    private $tmp_name;
    private $type;
    private $size;

    private $target_dir;
    private $target_file;

    /**
     * Galery constructor.
     */
    public function __construct($target_dir)
    {
        $this->setTargetDir($target_dir);
        $this->table = strtolower(get_class());
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getSrc()
    {
        return $this->src;
    }

    /**
     * @param mixed $link
     */
    public function setSrc($src)
    {
        $this->src = $src;
    }

    /**
     * @return mixed
     */
    public function getCreateDate()
    {
        return $this->create_date;
    }

    /**
     * @return mixed
     */
    public function getHunCreateDate()
    {
        $converter = new DateToHunDatum($this->create_date);
        return $converter->getDatum();
    }

    /**
     * @param mixed $create_date
     */
    public function setCreateDate($create_date)
    {
        $this->create_date = $create_date;
    }

    /**
     * @return mixed
     */
    public function getGaleryId()
    {
        return $this->galery_id;
    }

    /**
     * @param mixed $category
     */
    public function setGaleryId($galery_id)
    {
        $this->galery_id = $galery_id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getTmpName()
    {
        return $this->tmp_name;
    }

    /**
     * @param mixed $tmp_name
     */
    public function setTmpName($tmp_name)
    {
        $this->tmp_name = $tmp_name;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param mixed $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }



    /**
     * @return mixed
     */
    public function getTargetDir()
    {
        return $this->target_dir;
    }

    /**
     * @param mixed $target_dir
     */
    public function setTargetDir($target_dir)
    {
        $this->target_dir = $target_dir;
    }

    /**
     * @return array
     */
    public function getAssocValues()
    {
        return array(
            'title' => $this->getTitle(),
            'src' => $this->getSrc(),
            'create_date' => $this->getCreateDate(),
            'galery_id' => $this->getGaleryId(),
            'active' => $this->getActive(),
            'deleted' => $this->getDeleted()
        );
    }

    /**
     * @return array
     */
    public function getValues()
    {
        return array(
            $this->getTitle(),
            $this->getSrc(),
            $this->getCreateDate(),
            $this->getGaleryId(),
            $this->getActive(),
            $this->getDeleted(),
            $this->getId()
        );
    }

    /**
     * @return string
     */
    public function getSql()
    {
        return "UPDATE photo SET
                    title=?,
                    src=?,
                    create_date=?,
                    galery_id=?,
                    active=?,
                    deleted=?
                    WHERE id=?
              ";
    }

    public function copy(){
        $this->target_file = $this->target_dir . basename($this->name);
        if (!copy($this->tmp_name, $this->target_file)) {
            return true;
        }else{
            return false;
        }
    }

    public function delete($pdo){

        $sql ="DELETE FROM photo WHERE id=?";
        $sth = $pdo->prepare($sql);
        return $sth->execute(array($this->getId()));
    }
}
?>