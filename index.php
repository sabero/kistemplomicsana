<?php

  /*** includes ***/
  include_once 'error_handler.php';
  include_once 'ChromePhp.php';

  if(session_id() == '') session_start();

  ini_set('max_execution_time', 900); //300 seconds = 5 minutes
  ini_set('memory_limit', '128M');

  $data = $_POST['data'];
  $images = $data['images'];
  //print_r($data);

  /*** initialize user logged in***/
  if(isset($_SESSION['logged']) && ($_SESSION['logged']=='LOGGED')){
    $logged = true;
    $loggedUserId = $_SESSION['loggedUserId'];
  }else{
    $logged = false;
    $loggedUserId = null;
  }

/*** initialize site type ***/
if(isset($_REQUEST['site'])){
  $siteType = $_REQUEST['site'];
}else{
  $siteType = 'site';
}

/*** initialize csana ***/
  if(isset($_REQUEST['csana'])){
    $csana = $_REQUEST['csana'];
  }else{
    $csana = 'pillango';
  }

  /*** initialize modul ***/
  if(isset($_REQUEST['modul'])){
   $modul = $_REQUEST['modul'];
  }else{
   $modul = 'home';
  }

  /*** initialize controller ***/
  if(isset($_REQUEST['controller'])){
   $controller = $_REQUEST['controller'];
  }else{
   $controller = 'homeController';
  }

  /*** initialize action ***/
  if(isset($_REQUEST['action'])){
   $action = $_REQUEST['action'];
    if($action == 'kijelentkezes') $action= 'logout';
  }else{
   $action = 'index';
  }

  /*** initialize action ***/
  if(isset($_REQUEST['model'])){
    $model = $_REQUEST['model'];
  }else{
    $model = 'homeModel';
  }

  /*** initialize action ***/
  if(isset($_REQUEST['id'])){
    $id = $_REQUEST['id'];
  }else{
    $id = 0;
  }

  /*** initialize login e-mail ***/
  if(isset($data['login_email'])){
    $login_email = $data['login_email'];
  }else{
    $login_email = 'GestUser';
  }

  /*** initialize login Password ***/
  if(isset($data['login_pwd'])){
    $login_pwd = $data['login_pwd'];
  }else{
    $login_pwd = 'GestUser';
  }

  /*** initialize login Password ***/
  if(isset($data['login_admin'])){
    $login_admin = $data['login_admin'];
  }else{
    $login_admin = false;
  }

  /*** initialize login e-mail ***/
  if(isset($data['signup_username'])){
    $signup_username = $data['signup_username'];
  }else{
    $signup_username = '';
  }

  /*** initialize login e-mail ***/
  if(isset($data['signup_email'])){
    $signup_email = $data['signup_email'];
  }else{
    $signup_email = '';
  }

  /*** initialize login Password ***/
  if(isset($data['signup_pwd'])){
    $signup_pwd = $data['signup_pwd'];
  }else{
    $signup_pwd = '';
  }

  /*** initialize SendMsg sender_name ***/
  if(isset($data['sender_name'])){
    $sender_name = $data['sender_name'];
  }else{
    $sender_name = '';
  }

  /*** initialize SendMsg sender_email ***/
  if(isset($data['sender_email'])){
    $sender_email = $data['sender_email'];
  }else{
    $sender_email = '';
  }

  /*** initialize SendMsg subject ***/
  if(isset($data['subject'])){
    $subject = $data['subject'];
  }else{
    $subject = '';
  }

  /*** initialize SendMsg message ***/
  if(isset($data['message'])){
    $message = $data['message'];
  }else{
    $message = '';
  }


  /*** define the site path ***/
  $site_path = realpath(dirname(__FILE__));
  define ('__SITE_PATH', $site_path);

  if($siteType == 'admin'){
    $pathPrefix = '../';
  }else{
    $pathPrefix = '';
  }

  includes($pathPrefix);

  /*** a new registry object ***/
  $registry = new Registry();

  /*** host (0: host = localehost)***/
  $host = 0;

  /*** a new config object ***/
  $config = new Config($host);

  /*** create the database registry object ***/
  $registry->db = Db::getInstance($config->dbName, $config->dbUser, $config->dbPass);

  $config->SITE_TYPE = $siteType;
  $config->PATH_PREFIX = $pathPrefix;

  //Bejelentkez�s
  $config->login_email = $login_email;
  $config->login_pwd = $login_pwd;
  $config->login_admin = $login_admin;
  $config->logged = $logged;
  $config->loggedUserId = $loggedUserId;

  //Regisztr�ci�
  $config->signup_username = $signup_username;
  $config->signup_email = $signup_email;
  $config->signup_pwd = $signup_pwd;

  //�zenet k�ld�se
  $config->sender_name = $sender_name;
  $config->sender_email = $sender_email;
  $config->subject = $subject;
  $config->message = $message;

  //szerkeztés
  $config->id = $id;

  //új objektum vagy objektum szerkesztés
  $config->POSTED_DATA = $data;
  $config->POSTED_IMAGES = $images;

  /*** load the router ***/
  $registry->router = new Router($registry, $config);

  /*** set the controller path ***/
  $registry->router->setPath ($pathPrefix.'modul/' . $modul .'/', $csana, $modul, $controller, $action, $model);

  /*** load up the template ***/
  $registry->template = new Template($registry);
  /*** load the controller ***/
  $registry->router->controllerLoader();


  function includes($pathPrefix){

    /*** include the controller class ***/
    include_once $pathPrefix.'application/Controller_base.class.php';

    /*** include the controller class ***/
    include_once $pathPrefix.'application/Model_base.class.php';

    /*** include $pathPrefix.the registry class ***/
    include_once $pathPrefix.'application/Registry.class.php';

    /*** include the router class ***/
    include_once $pathPrefix.'application/Router.class.php';

    /*** include the template class ***/
    include_once $pathPrefix.'application/Template.class.php';

    /*** include the config class ***/
    include_once $pathPrefix.'application/Config.class.php';

    /*** include the template class ***/
    include_once $pathPrefix.'application/Db.class.php';
  }

?>
