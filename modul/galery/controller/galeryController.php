<?php

Class galeryController Extends baseController {


    public function index() {


        $images_count = $_POST['images_count'];
        $images = array();

        for($i = 0; $i < $images_count; $i++){
            $image_name  = 'image'.$i;
            $fileName = $_FILES[$image_name]['name'];
            $fileType = $_FILES[$image_name]['type'];
            $tmp_name = $_FILES[$image_name]['tmp_name'];
            $fileSize = $_FILES[$image_name]['size'];
            $fileError = $_FILES[$image_name]['error'];
            //$fileContent = file_get_contents($tmp_name);
            //$dataUrl = 'data:' . $fileType . ';base64,' . base64_encode($fileContent);

            $new_tmp_name = $this->uploadToTMP($fileName, $tmp_name, $fileSize, $fileType);
            if(null != $new_tmp_name){
                $image = array(
                    'name' => $fileName,
                    'tmp_name' => $new_tmp_name,
                    'type' => $fileType,
                    'size' => $fileSize,
                    'error' => $fileError
                );
                $images[$i] = $image;
            }

        }

        unset($_SESSION['images']);
        $json = json_encode($images);
        $_SESSION['images'] = $json;
        echo $_SESSION['images'];

    }

    private function uploadToTMP($name, $tmp_name, $size, $type){



        /*$files = glob($this->config->PATH_PREFIX.'resources/img/tmp/*'); // get all file names
        foreach($files as $file){ // iterate files
            if(is_file($file))
                unlink($file); // delete file
        }*/

        $target_dir = $this->config->PATH_PREFIX.'resources/img/tmp/';
        $target_file = $target_dir . basename($name);
        $uploadOk = 1;
        $errors = array();
        $error_index = 0;

        if(isset($_POST["submit"])) {
            $check = getimagesize($tmp_name);
            if($check !== false) {
                $uploadOk = 1;
            } else {
                $errors[$error_index] = "File is not an image.";
                $error_index++;
                $uploadOk = 0;
            }
        }
        // Check if file already exists
        if (file_exists($target_file)) {
            $errors[$error_index] = "Sorry, file already exists.";
            $error_index++;
            $uploadOk = 0;
        }
        // Check file size
        /*if ($size > 99000000) {
            $errors[$error_index] = "Sorry, your file is too large.";
            $error_index++;
            $uploadOk = 0;
        }*/
        // Allow certain file formats
        if($type != "image/jpg" && $type != "image/png" && $type != "image/jpeg") {
            $errors[$error_index] = "Sorry, only JPG, JPEG, PNG files are allowed.";
            $error_index++;
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            $errors[$error_index] = "Sorry, your file was not uploaded.";
            $error_index++;
            // if everything is ok, try to upload file
        } else {

            if (move_uploaded_file($tmp_name, $target_file)) {
                $this->message = "The file ". basename( $name). " has been uploaded.";
                return $target_file;
            }else{
                $errors[$error_index] = "Sorry, there was an error uploading your file.";
                $error_index++;
                ChromePhp::log($errors);
                return null;
            }
            /*list($width, $height) = getimagesize($tmp_name);
            if ($width < 970 || $height < 730) {

                if (move_uploaded_file($tmp_name, $target_file)) {
                    $this->message = "The file ". basename( $name). " has been uploaded.";
                    return $target_file;
                }else{
                    $errors[$error_index] = "Sorry, there was an error uploading your file.";
                    $error_index++;
                    ChromePhp::log($errors);
                    return null;
                }

            }else if($this->resize(960, 720, $tmp_name, $type, $target_file)){
                $this->message = "The file ". basename( $name). " has been uploaded.";
                return $target_file;
            } else {
                $errors[$error_index] = "Sorry, there was an error uploading your file.";
                $error_index++;
                ChromePhp::log($errors);
                return null;

            }*/
        }
    }


    /**
     * Image resize
     * @param int $width
     * @param int $height
     */
    function resize($width, $height, $temp_name, $type, $path){
        /* Get original image x y*/
        list($w, $h) = getimagesize($temp_name);
        /* calculate new image size with ratio */
        $ratio = max($width/$w, $height/$h);
        $h = ceil($height / $ratio);
        $x = ($w - $width / $ratio) / 2;
        $y = ($h-$height) / 2;
        $w = ceil($width / $ratio);
        /* new file name */
        //$path = 'uploads/'.$width.'x'.$height.'_'.$name;
        /* read binary data from image file */
        $imgString = file_get_contents($temp_name);
        /* create image from string */
        $image = imagecreatefromstring($imgString);
        $tmp = imagecreatetruecolor($width, $height);
        imagecopyresampled($tmp, $image, 0, $y, $x, 0, $width, $height, $w, $h);
        /* Save image */

        if($type == 'image/jpeg' && imagejpeg($tmp, $path, 70)){
            /* cleanup memory */
            imagedestroy($image);
            imagedestroy($tmp);
            ChromePhp::log('Resize success!!!!!');
            return true;
        }else if($type == 'image/png' && imagepng($tmp, $path, 70)){
            /* cleanup memory */
            imagedestroy($image);
            imagedestroy($tmp);
            ChromePhp::log('Resize success!!!!!');
            return true;
        }else{
            /* cleanup memory */
            imagedestroy($image);
            imagedestroy($tmp);
            return false;
        }


    }


}

?>
