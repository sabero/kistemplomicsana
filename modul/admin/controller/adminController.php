<?php

Class adminController Extends baseController {

    public $twig = null;


    public function index() {
        $this->model->setTemplate('admin_index.twig');
        $this->initialize();
        $this->model->assignedList->assign('adminContent', 'admin_dashboard.twig');
        $this->showPage();
    }

    /****** User ***********************************************************************************************************************************/

    public function getUserList(){
        $this->model->setTemplate('admin_index.twig');
        $this->initialize();
        $userList = $this->model->getUserList();
        $this->model->assignedList->assign('userList', $userList);
        $this->model->assignedList->assign('adminContent', 'admin_user_list.twig');
        $this->showPage();
    }

    public function editUser(){
        $this->model->setTemplate('admin_index.twig');
        $this->initialize();
        $user = $this->model->getUserById($this->config->id);
        $this->model->assignedList->assign('user', $user);
        $this->model->assignedList->assign('adminContent', 'admin_user_edit.twig');
        $this->showPage();
    }

    public function updateUser(){

        $update = $this->model->updateUser();
        if($update){
            echo '{"error":"", "message":"Sikeres adat mentés!", "url":"'.$this->config->ADMIN_DOMAIN.'/felhasznalo/szerkesztes/'.$this->config->id.'"}';
        }else{
            echo '{"error":"1", "message":"Nincs változás!", "url":"'.$this->config->ADMIN_DOMAIN.'/felhasznalo/szerkesztes/'.$this->config->id.'"}';
        }
    }

    public function deleteUser(){

        $delete = $this->model->delete('User', $this->config->id);
        if($delete){
            echo '{"error":"", "message":"Sikeres törlés!", "url":"'.$this->config->ADMIN_DOMAIN.'/felhasznalo/"}';
        }else{
            echo '{"error":"1", "message":"Hiba történt a törlés során!", "url":"'.$this->config->ADMIN_DOMAIN.'/felhasznalo/"}';
        }
    }



    /******* Email ***********************************************************************************************************************/

    public function getEmailList(){
        $this->model->setTemplate('admin_index.twig');
        $this->initialize();
        $emailList = $this->model->getEmailList();
        $this->model->assignedList->assign('emailList', $emailList);
        $this->model->assignedList->assign('adminContent', 'admin_email_list.twig');
        $this->showPage();
    }

    public function editEmail(){
        $this->model->setTemplate('admin_index.twig');
        $this->initialize();
        if($this->config->id > 0){
            $email = $this->model->getObjectById('Email_list', $this->config->id);
            //print_r($email);
            $this->model->assignedList->assign('email', $email);
        }
        $this->model->assignedList->assign('adminContent', 'admin_email_edit.twig');
        $this->showPage();
    }

    public function newEmail(){
        $this->model->setTemplate('admin_index.twig');
        $this->initialize();
        $this->model->assignedList->assign('adminContent', 'admin_email_new.twig');
        $this->showPage();
    }

    public function insertEmail(){

        $insert = $this->model->insertEmail();
        if($insert){
            echo '{"error":"", "message":"Sikeres új adat rekord hozzáadása!", "url":"'.$this->config->ADMIN_DOMAIN.'/uj-email/"}';
        }else{
            echo '{"error":"1", "message":"Hiba történt új adat rekord hozzáadása során! Kérem, próbáld újra!", "url":"'.$this->config->ADMIN_DOMAIN.'/uj-email/"}';
        }
    }

    public function updateEmail(){

        $update = $this->model->updateEmail();
        if($update){
            echo '{"error":"", "message":"Sikeres adat mentés!", "url":"'.$this->config->ADMIN_DOMAIN.'/email/szerkesztes/'.$this->config->id.'"}';
        }else{
            echo '{"error":"1", "message":"Nincs változás!", "url":"'.$this->config->ADMIN_DOMAIN.'/email/szerkesztes/'.$this->config->id.'"}';
        }
    }

    public function deleteEmail(){

        $delete = $this->model->delete('Email_list', $this->config->id);
        if($delete){
            echo '{"error":"", "message":"Sikeres törlés!", "url":"'.$this->config->ADMIN_DOMAIN.'/email/"}';
        }else{
            echo '{"error":"1", "message":"Hiba történt a törlés során!", "url":"'.$this->config->ADMIN_DOMAIN.'/email/"}';
        }
    }


    /******* Galery category ***********************************************************************************************************************/

    public function getGaleryCategoryList(){
        $this->model->setTemplate('admin_index.twig');
        $this->initialize();
        $galeryCategoryList = $this->model->getGaleryCategoryList();
        $this->model->assignedList->assign('galeryCategoryList', $galeryCategoryList);
        $this->model->assignedList->assign('adminContent', 'admin_galery_category_list.twig');
        $this->showPage();
    }

    public function editGaleryCategory(){
        $this->model->setTemplate('admin_index.twig');
        $this->initialize();
        if($this->config->id > 0){
            $galeryCategory = $this->model->getObjectById('Galery_category', $this->config->id);
            //print_r($email);
            $this->model->assignedList->assign('galeryCategory', $galeryCategory);
        }
        $this->model->assignedList->assign('adminContent', 'admin_galery_category_edit.twig');
        $this->showPage();
    }

    public function newGaleryCategory(){
        $this->model->setTemplate('admin_index.twig');
        $this->initialize();
        $this->model->assignedList->assign('adminContent', 'admin_galery_category_new.twig');
        $this->showPage();
    }

    public function insertGaleryCategory(){

        $insert = $this->model->insertGaleryCategory();
        if($insert){
            echo '{"error":"", "message":"Sikeres új adat rekord hozzáadása!", "url":"'.$this->config->ADMIN_DOMAIN.'/uj-galeria-kategoria/"}';
        }else{
            echo '{"error":"1", "message":"Hiba történt új adat rekord hozzáadása során! Kérem, próbáld újra!", "url":"'.$this->config->ADMIN_DOMAIN.'/uj-galeria-kategoria/"}';
        }
    }

    public function updateGaleryCategory(){

        $update = $this->model->updateGaleryCategory();
        if($update){
            echo '{"error":"", "message":"Sikeres adat mentés!", "url":"'.$this->config->ADMIN_DOMAIN.'/galeria-kategoria/szerkesztes/'.$this->config->id.'"}';
        }else{
            echo '{"error":"1", "message":"Nincs változás!", "url":"'.$this->config->ADMIN_DOMAIN.'/galeria-kategoria/szerkesztes/'.$this->config->id.'"}';
        }
    }

    public function deleteGaleryCategory(){

        $delete = $this->model->delete('Galery_category', $this->config->id);
        if($delete){
            echo '{"error":"", "message":"Sikeres törlés!", "url":"'.$this->config->ADMIN_DOMAIN.'/galeria-kategoria/"}';
        }else{
            echo '{"error":"1", "message":"Hiba történt a törlés során!", "url":"'.$this->config->ADMIN_DOMAIN.'/galeria-kategoria/"}';
        }
    }


    /******* Galéria ****************************************************************************************************************/

    public function getGaleryList(){
        $this->model->setTemplate('admin_index.twig');
        $this->initialize();
        $galeryList = $this->model->getGaleryList();
        $this->model->assignedList->assign('galeryList', $galeryList);
        $this->model->assignedList->assign('adminContent', 'admin_galery_list.twig');
        $this->showPage();
    }

    public function editGalery(){
        $this->model->setTemplate('admin_index.twig');
        $this->initialize();
        if($this->config->id > 0){
            $galeria = $this->model->getObjectById('Galery', $this->config->id);
            $this->model->assignedList->assign('galeria', $galeria);
        }
        $this->model->assignedList->assign('adminContent', 'admin_galery_edit.twig');
        $this->showPage();
    }

    public function newGalery(){
        $this->model->setTemplate('admin_index.twig');
        $this->initialize();
        $galeryCategoryList = $this->model->getGaleryCategoryList();
        $this->model->assignedList->assign('galeryCategoryList', $galeryCategoryList);
        $this->model->assignedList->assign('adminContent', 'admin_galery_new.twig');
        $this->showPage();
    }

    public function insertGalery(){

        $insert = $this->model->insertGalery();

        if($insert){
            echo '{"error":"", "message":"Sikeres új galéria rekord hozzáadása!", "url":"'.$this->config->ADMIN_DOMAIN.'/galeria/"}';
        }else{
            echo '{"error":"1", "message":"Hiba történt új galéria rekord hozzáadása során! Kérem, próbáld újra!", "url":"'.$this->config->ADMIN_DOMAIN.'/uj-galeria/"}';
        }
    }

    public function updateGalery(){

        $update = $this->model->updateGalery();
        if($update){
            echo '{"error":"", "message":"Sikeres adat mentés!", "url":"'.$this->config->ADMIN_DOMAIN.'/galeria/szerkesztes/'.$this->config->id.'"}';
        }else{
            echo '{"error":"1", "message":"Nincs változás!", "url":"'.$this->config->ADMIN_DOMAIN.'/galeria/szerkesztes/'.$this->config->id.'"}';
        }
    }

    public function deleteGalery(){

        $delete = $this->model->deleteGalery($this->config->id);
        if($delete){
            echo '{"error":"", "message":"Sikeres törlés!", "url":"'.$this->config->ADMIN_DOMAIN.'/galeria/"}';
        }else{
            echo '{"error":"1", "message":"Hiba történt a törlés során!", "url":"'.$this->config->ADMIN_DOMAIN.'/galeria/"}';
        }
    }

    /******* Photo ************************************************************************************************************************/

    public function getPhotoList(){
        $this->model->setTemplate('admin_index.twig');
        $this->initialize();
        $photoList = $this->model->getPhotoList();
        $this->model->assignedList->assign('photoList', $photoList);
        $this->model->assignedList->assign('adminContent', 'admin_photo_list.twig');
        $this->showPage();
    }

    public function deletePhoto(){

        $delete = $this->model->deletePhoto($this->config->id);
        if($delete){
            echo '{"error":"", "message":"Sikeres törlés!", "url":"'.$this->config->ADMIN_DOMAIN.'/photo/"}';
        }else{
            echo '{"error":"1", "message":"Hiba történt a törlés során!", "url":"'.$this->config->ADMIN_DOMAIN.'/photo/"}';
        }
    }


    /******* Blog ****************************************************************************************************************/

    public function getBlogList(){

        $this->model->setTemplate('admin_index.twig');
        $this->initialize();
        $blogList = $this->model->getBlogList();
        $this->model->assignedList->assign('blogList', $blogList);
        $this->model->assignedList->assign('adminContent', 'admin_blog_list.twig');
        $this->showPage();
    }

    public function editBlog(){

        $this->model->setTemplate('admin_index.twig');
        $this->initialize();
        if($this->config->id > 0){
            $blog = $this->model->getObjectById('Blog', $this->config->id);
            $this->model->assignedList->assign('blog', $blog);
        }
        $galeryList = $this->model->getGaleryList();
        $this->model->assignedList->assign('galeryList', $galeryList);
        $this->model->assignedList->assign('adminContent', 'admin_blog_edit.twig');
        $this->showPage();
    }

    public function newBlog(){

        $this->model->setTemplate('admin_index.twig');
        $this->initialize();
        $galeryList = $this->model->getGaleryList();
        $this->model->assignedList->assign('galeryList', $galeryList);
        $this->model->assignedList->assign('adminContent', 'admin_blog_new.twig');
        $this->showPage();
    }

    public function insertBlog(){


        $insert = $this->model->insertBlog();

        if($insert){
            echo '{"error":"", "message":"Sikeres új galéria rekord hozzáadása!", "url":"'.$this->config->ADMIN_DOMAIN.'/blog/"}';
        }else{
            echo '{"error":"1", "message":"Hiba történt új galéria rekord hozzáadása során! Kérem, próbáld újra!", "url":"'.$this->config->ADMIN_DOMAIN.'/uj-blog/"}';
        }
    }

    public function updateBlog(){

        $update = $this->model->updateBlog();
        if($update){
            echo '{"error":"", "message":"Sikeres adat mentés!", "url":"'.$this->config->ADMIN_DOMAIN.'/blog/szerkesztes/'.$this->config->id.'"}';
        }else{
            echo '{"error":"1", "message":"Nincs változás!", "url":"'.$this->config->ADMIN_DOMAIN.'/blog/szerkesztes/'.$this->config->id.'"}';
        }
    }

    public function deleteBlog(){

        $delete = $this->model->deleteBlog($this->config->id);
        if($delete){
            echo '{"error":"", "message":"Sikeres törlés!", "url":"'.$this->config->ADMIN_DOMAIN.'/blog/"}';
        }else{
            echo '{"error":"1", "message":"Hiba történt a törlés során!", "url":"'.$this->config->ADMIN_DOMAIN.'/blog/"}';
        }
    }


    /******* Tartalom ****************************************************************************************************************/

    public function getContentList(){

        $this->model->setTemplate('admin_index.twig');
        $this->initialize();
        $contentList = $this->model->getContentList();
        $this->model->assignedList->assign('contentList', $contentList);
        $this->model->assignedList->assign('adminContent', 'admin_content_list.twig');
        $this->showPage();
    }

    public function editContent(){

        $this->model->setTemplate('admin_index.twig');
        $this->initialize();
        if($this->config->id > 0){
            $content = $this->model->getObjectById('Content', $this->config->id);
            $this->model->assignedList->assign('content', $content);
        }
        $contentList = $this->model->getContentList();
        $this->model->assignedList->assign('contentList', $contentList);
        $this->model->assignedList->assign('adminContent', 'admin_content_edit.twig');
        $this->showPage();
    }

    public function newContent(){

        $this->model->setTemplate('admin_index.twig');
        $this->initialize();
        $contentList = $this->model->getContentList();
        $this->model->assignedList->assign('contentList', $contentList);
        $this->model->assignedList->assign('adminContent', 'admin_content_new.twig');
        $this->showPage();
    }

    public function insertContent(){

        $insert = $this->model->insertContent();
        if($insert){
            echo '{"error":"", "message":"Sikeres új galéria rekord hozzáadása!", "url":"'.$this->config->ADMIN_DOMAIN.'/tartalom/"}';
        }else{
            echo '{"error":"1", "message":"Hiba történt új galéria rekord hozzáadása során! Kérem, próbáld újra!", "url":"'.$this->config->ADMIN_DOMAIN.'/uj-tartalom/"}';
        }
    }

    public function updateContent(){

        $update = $this->model->updateContent();
        if($update){
            echo '{"error":"", "message":"Sikeres adat mentés!", "url":"'.$this->config->ADMIN_DOMAIN.'/tartalom/szerkesztes/'.$this->config->id.'"}';
        }else{
            echo '{"error":"1", "message":"Nincs változás!", "url":"'.$this->config->ADMIN_DOMAIN.'/tartalom/szerkesztes/'.$this->config->id.'"}';
        }
    }

    public function deleteContent(){

        $delete = $this->model->delete('Content', $this->config->id);
        if($delete){
            echo '{"error":"", "message":"Sikeres törlés!", "url":"'.$this->config->ADMIN_DOMAIN.'/tartalom/"}';
        }else{
            echo '{"error":"1", "message":"Hiba történt a törlés során!", "url":"'.$this->config->ADMIN_DOMAIN.'/tartalom/"}';
        }
    }

}

?>
