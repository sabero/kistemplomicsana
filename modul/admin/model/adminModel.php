<?php

/**
 * Created by PhpStorm.
 * User: Ouechtati Saber
 * Date: 2015.10.04.
 * Time: 15:40
 */
class adminModel extends baseModel
{

    /********  User **********************************************************************************************************************/

    public function getUserList(){

        include_once $this->config->PATH_PREFIX.'objects/User.php';
        $this->tableName = 'user';
        $this->bindList = '*';
        $this->where = ' WHERE active = 1 AND deleted = 0 ';
        //$this->join = ' LEFT JOIN email_list ON user.email=email_list.email ';
        return $this->select('User');
    }


    public function updateUser(){
        include_once $this->config->PATH_PREFIX.'objects/User.php';
        $user = $this->getUserById($this->config->id);
        $data = $this->config->POSTED_DATA;
        $user->setName($data['name']);
        $user->setEmail($data['email']);
        if($user->update($this->pdo) > 0){
            return true;
        }else{
            return false;
        }
    }


    /********  Email **********************************************************************************************************************/


    public function getEmailList(){

        include_once $this->config->PATH_PREFIX.'objects/Email_list.php';
        $this->tableName = 'email_list';
        $this->bindList = '*';
        $this->where = ' WHERE active = 1 AND deleted = 0 ';
        return $this->select('User');
    }

    public function insertEmail(){
        include_once $this->config->PATH_PREFIX.'objects/Email_list.php';
        $email = new Email_list();
        $data = $this->config->POSTED_DATA;
        $email->setName($data['name']);
        $email->setEmail($data['email']);
        $email->setChildName($data['child_name']);
        $email->setGroupName($data['group_name']);
        if($email->insert($this->pdo) != null){
            return true;
        }else{
            return false;
        }
    }

    public function updateEmail(){
        include_once $this->config->PATH_PREFIX.'objects/Email_list.php';
        $email = $this->getObjectById('Email_list', $this->config->id);
        $data = $this->config->POSTED_DATA;
        $email->setName($data['name']);
        $email->setEmail($data['email']);
        $email->setChildName($data['child_name']);
        $email->setGroupName($data['group_name']);
        if($email->update($this->pdo) > 0){
            return true;
        }else{
            return false;
        }
    }


    /********  Galery Category **********************************************************************************************************************/


    public function getGaleryCategoryList(){

        include_once $this->config->PATH_PREFIX.'objects/Galery_category.php';
        $this->tableName = 'galery_category';
        $this->bindList = '*';
        $this->where = ' WHERE active = 1 AND deleted = 0 ';
        return $this->select('Galery_category');
    }

    public function getGaleryCategotyTitleById($id){
        include_once $this->config->PATH_PREFIX.'objects/Galery_category.php';
        $category = $this->getObjectById('Galery_category', $id);
        return $category->getTitle();
    }

    public function getGaleryCategotyLinkById($id){
        include_once $this->config->PATH_PREFIX.'objects/Galery_category.php';
        $category = $this->getObjectById('Galery_category', $id);
        return $category->getLink();
    }

    public function insertGaleryCategory(){
        include_once $this->config->PATH_PREFIX.'objects/Galery_category.php';
        $galeryCategory = new Galery_category();
        $data = $this->config->POSTED_DATA;
        $galeryCategory->setTitle($data['title']);
        $galeryCategory->setLink($data['link']);
        $galeryCategory->setCreateDate(date("Y-m-d H:i:s"));
        if($galeryCategory->insert($this->pdo) != null){
            return true;
        }else{
            return false;
        }
    }

    public function updateGaleryCategory(){
        include_once $this->config->PATH_PREFIX.'objects/Galery_category.php';
        $galeryCategory = $this->getObjectById('Galery_category', $this->config->id);
        $data = $this->config->POSTED_DATA;
        $galeryCategory->setTitle($data['title']);
        $galeryCategory->setLink($data['link']);
        if($galeryCategory->update($this->pdo) > 0){
            return true;
        }else{
            return false;
        }
    }


    /********  Galery **********************************************************************************************************************/

    public function getGaleryList(){

        include_once $this->config->PATH_PREFIX.'objects/Galery.php';
        $this->tableName = 'galery';
        $this->bindList = '*';
        $this->where = ' WHERE active = 1 AND deleted = 0 ';
        $list = $this->select('Galery');
        return $list;

    }

    public function getGaleryById($id){
        include_once $this->config->PATH_PREFIX.'objects/Galery.php';
        return $this->getObjectById('Galery', $id);
    }

    public function getGaleryTitleById($id){
        include_once $this->config->PATH_PREFIX.'objects/Galery.php';
        $galery = $this->getObjectById('Galery', $id);
        return $galery->getTitle();
    }

    public function getGaleryLinkById($id){
        include_once $this->config->PATH_PREFIX.'objects/Galery.php';
        $galery = $this->getObjectById('Galery', $id);
        return $galery->getLink();
    }

    public function insertGalery(){

        $images = json_decode($_SESSION['images'],true);
        $category_id = $_POST['category_id'];
        $category_link = $this->getGaleryCategotyLinkById($category_id);
        $title = $_POST['title'];
        $galery_link = $_POST['link'];
        $text = $_POST['text'];
        $images_count = $_POST['image_count'];

        include_once $this->config->PATH_PREFIX.'objects/Photo.php';
        include_once $this->config->PATH_PREFIX.'objects/Galery.php';

        $galery_path = $this->config->PATH_PREFIX.'resources/img/galeria/'.$category_link.'/'.$galery_link;

        $galery = new Galery();
        $galery->setTitle($title);
        $galery->setText($text);
        $galery->setLink($galery_link);
        $galery->setCreateDate(date("Y-m-d H:i:s"));
        $galery->setCategoryId($category_id);
        $galery->setCategoryTitle($this->getGaleryCategotyTitleById($galery->getCategoryId()));

        if($galery->insert($this->pdo) > 0){

            if (!file_exists($galery_path)) {
                mkdir($galery_path, 0777, true);
            }

            for($i=0; $i < $images_count; $i++){
                $photo = new Photo($galery_path.'/');

                $image_type = $images[$i]['type'];
                //$image_name = $images[$i]['name'];
                $j = $i+1;
                $image_name = 'kep_'.$j;

                if($image_type == "image/png"){
                    $image_name .= '.png';
                }else if("image/jpg"){
                    $image_name .= '.jpg';
                }else{
                    $image_name .= '.jpeg';
                }

                $image_tmp_name = $images[$i]['tmp_name'];
                $image_size = $images[$i]['size'];

                $photo->setName($image_name);
                $photo->setTmpName($image_tmp_name);
                $photo->setType($image_type);
                $photo->setSize($image_size);
                $photo->setTitle($image_name);
                $photo->setSrc('pic/galeria/'.$category_link.'/'.$galery_link.'/'.$image_name);
                $photo->setCreateDate(date("Y-m-d H:i:s"));
                $photo->setGaleryId($galery->getLastId());

                $photo->copy();
                $photo->insert($this->pdo);
            }

            $files = glob($this->config->PATH_PREFIX.'resources/img/tmp/*'); // get all file names
            foreach($files as $file){ // iterate files
                if(is_file($file))
                    unlink($file); // delete file
            }
            return true;
        }else{
            return false;
        }
    }

    public function deleteGalery($id){

        $galery = $this->getGaleryById($id);
        if($galery->delete($this->pdo)){

            $category_link = $this->getGaleryCategotyLinkById($galery->getCategoryId());
            $galeryDir = $this->config->PATH_PREFIX.'resources/img/galeria/'.$category_link.'/'.$galery->getLink();
            $it = new RecursiveDirectoryIterator($galeryDir, RecursiveDirectoryIterator::SKIP_DOTS);
            $files = new RecursiveIteratorIterator($it, RecursiveIteratorIterator::CHILD_FIRST);
            foreach($files as $file) {
                if ($file->isDir()){
                    rmdir($file->getRealPath());
                } else {
                    unlink($file->getRealPath());
                }
            }
            rmdir($galeryDir);
            return true;
        }else{
            return false;
        }
    }


    /********  Photo **********************************************************************************************************************/

    public function getPhotoList(){

        include_once $this->config->PATH_PREFIX.'objects/Photo.php';
        $this->tableName = 'photo';
        $this->bindList = '*';
        $this->where = ' WHERE active = 1 AND deleted = 0 ';
        $this->orderBy = ' ORDER BY create_date ';
        return $this->select('Photo');
    }

    public function getPhotoById($id){
        include_once $this->config->PATH_PREFIX.'objects/Photo.php';
        return $this->getObjectById('Photo', $id);
    }

    public function deletePhoto($id){

        $photo = $this->getPhotoById($id);
        $galery = $this->getGaleryById($photo->getGaleryId());
        $category_link = $this->getGaleryCategotyLinkById($galery->getCategoryId());
        if($photo->delete($this->pdo)){
            $file = $this->config->PATH_PREFIX.'resources/img/galeria/'.$category_link.'/'.$galery->getLink().'/'.$photo->getTitle();
            if(is_file($file)){
                unlink($file);
            }
            return true;
        }else{
            return false;
        }
    }


    /********  Blog **********************************************************************************************************************/

    public function getBlogList(){

        include_once $this->config->PATH_PREFIX.'objects/Blog.php';
        $this->tableName = 'blog';
        $this->bindList = '*';
        $this->where = ' WHERE active = 1 AND deleted = 0 ';
        $list = $this->select('Blog');
        return $list;

    }

    public function getBlogGaleryList(){
        include_once $this->config->PATH_PREFIX.'objects/Galery.php';
        $this->tableName = 'galery';
        $this->bindList = '*';
        $this->where = ' WHERE category_id = 1 AND active = 1 AND deleted = 0 ';
        $list = $this->select('Galery');
        return $list;
    }

    public function getBlogById($id){
        include_once $this->config->PATH_PREFIX.'objects/Blog.php';
        return $this->getObjectById('Blog', $id);
    }

    public function getBlogTitleById($id){
        include_once $this->config->PATH_PREFIX.'objects/Blog.php';
        $blog = $this->getObjectById('Blog', $id);
        return $blog->getTitle();
    }

    public function insertBlog(){

        $data = $_POST['data'];
        $title = $data['title'];
        $link = $data['link'];
        $text = $data['text'];
        $galery_id = $data['galery_id'];
        $galery_title = $this->getGaleryTitleById($galery_id);

        include_once $this->config->PATH_PREFIX.'objects/Blog.php';

        $blog = new Blog();
        $blog->setTitle($title);
        $blog->setText($text);
        $blog->setLink($link);
        $blog->setCreateDate(date("Y-m-d H:i:s"));
        $blog->setGaleryId($galery_id);
        $blog->setGaleryTitle($galery_title);


        if($blog->insert($this->pdo) > 0){
            return true;
        }else{
            return false;
        }
    }

    public function updateBlog(){

        $data = $_POST['data'];
        $title = $data['title'];
        $link = $data['link'];
        $text = $data['text'];
        $galery_id = $data['galery_id'];
        $galery_title = $this->getGaleryTitleById($galery_id);

        ChromePhp::log('id: '.$this->config->id);

        include_once $this->config->PATH_PREFIX.'objects/Blog.php';
        $blog = $this->getObjectById('Blog', $this->config->id);
        $blog->setTitle($title);
        $blog->setText($text);
        $blog->setLink($link);
        $blog->setCreateDate(date("Y-m-d H:i:s"));
        $blog->setGaleryId($galery_id);
        $blog->setGaleryTitle($galery_title);

        if($blog->update($this->pdo) > 0){
            return true;
        }else{
            return false;
        }
    }

    public function deleteBlog($id){

        $blog = $this->getBlogById($id);
        if($blog->delete($this->pdo)){
            return true;
        }else{
            return false;
        }
    }


    /********  Tartalom **********************************************************************************************************************/

    public function getContentList(){

        include_once $this->config->PATH_PREFIX.'objects/Content.php';
        $this->tableName = 'content';
        $this->bindList = '*';
        $this->where = ' WHERE active = 1 AND deleted = 0 ';
        $list = $this->select('Content');
        return $list;

    }


    public function getContentById($id){
        include_once $this->config->PATH_PREFIX.'objects/Content.php';
        return $this->getObjectById('Content', $id);
    }

    public function getContentTitleById($id){
        include_once $this->config->PATH_PREFIX.'objects/Content.php';
        $content = $this->getObjectById('Content', $id);
        return $content->getTitle();
    }

    public function insertContent(){

        $data = $_POST['data'];
        $title = $data['title'];
        $link = $data['link'];
        $text = $data['text'];

        include_once $this->config->PATH_PREFIX.'objects/Content.php';

        $content = new Content();
        $content->setTitle($title);
        $content->setText($text);
        $content->setLink($link);
        $content->setCreateDate(date("Y-m-d H:i:s"));

        if($content->insert($this->pdo) > 0){
            return true;
        }else{
            return false;
        }
    }

    public function updateContent(){

        $data = $_POST['data'];
        $title = $data['title'];
        $link = $data['link'];
        $text = $data['text'];

        include_once $this->config->PATH_PREFIX.'objects/Content.php';
        $content = $this->getObjectById('Content', $this->config->id);
        $content->setTitle($title);
        $content->setText($text);
        $content->setLink($link);
        $content->setCreateDate(date("Y-m-d H:i:s"));

        if($content->update($this->pdo) > 0){
            return true;
        }else{
            return false;
        }
    }

    public function deleteContent($id){

        $content = $this->getContentById($id);
        if($content->delete($this->pdo)){
            return true;
        }else{
            return false;
        }
    }




    /********  Object **********************************************************************************************************************/

    public function delete($class, $id){

        include_once $this->config->PATH_PREFIX.'objects/'.$class.'.php';
        //$objectClass = $class.'.php';
        $object = new $class();
        $object = $this->getObjectById($class, $id);
        $object->setDeleted(1);
        //print_r($object);
        if($object->update($this->pdo) > 0){
            return true;
        }else{
            return false;
        }
    }


}