<?php

/**
 * Created by PhpStorm.
 * User: Ouechtati Saber
 * Date: 2015.10.04.
 * Time: 15:40
 */
class csanaModel extends baseModel
{

     public function getSiteMapItemList(){

         $items = array();
         $items[0]['name'] = 'Bárány';
         $items[1]['name'] = 'Halacska';
         $items[2]['name'] = 'Madárka';
         $items[3]['name'] = 'Őzike';
         $items[4]['name'] = 'Pillango';

         $items[0]['_name'] = 'barany';
         $items[1]['_name'] = 'halacska';
         $items[2]['_name'] = 'madarka';
         $items[3]['_name'] = 'ozike';
         $items[4]['_name'] = 'pillango';

         return $items;
     }

    /********  Galery Category **********************************************************************************************************************/


    public function getGaleryCategoryList(){

        include_once $this->config->PATH_PREFIX.'objects/Galery_category.php';
        $this->tableName = 'galery_category';
        $this->bindList = '*';
        $this->where = ' WHERE active = 1 AND deleted = 0 ';
        $this->orderBy = ' ORDER BY id ASC';
        return $this->select('Galery_category');
    }



    /********  Galery **********************************************************************************************************************/

    public function getGaleryList(){

        include_once $this->config->PATH_PREFIX.'objects/Galery.php';
        $this->tableName = 'galery';
        $this->bindList = '*';
        $this->where = ' WHERE category_id != 1 AND active = 1 AND deleted = 0 ';
        $this->orderBy = ' ORDER BY id ASC';
        $list = $this->select('Galery');
        return $list;

    }



    public function getGaleryListByCategoryId($id){

        include_once $this->config->PATH_PREFIX.'objects/Galery.php';
        $this->tableName = 'galery';
        $this->bindList = '*';
        $this->where = ' WHERE category_id = '.$id.' AND active = 1 AND deleted = 0 ';
        $this->orderBy = ' ORDER BY id ASC';
        $list = $this->select('Galery');
        return $list;

    }

    public function getGaleryCategotyTitleById($id){
        include_once $this->config->PATH_PREFIX.'objects/Galery_category.php';
        $category = $this->getObjectById('Galery_category', $id);
        return $category->getTitle();
    }



    /********  Photo **********************************************************************************************************************/

    public function getPhotoList(){

        include_once $this->config->PATH_PREFIX.'objects/Photo.php';
        $this->tableName = 'photo';
        $this->bindList = '*';
        $this->where = ' WHERE active = 1 AND deleted = 0 ';
        $this->orderBy = ' ORDER BY id ASC';
        return $this->select('Photo');
    }

    public function getPhotoListByGaleryID($id){

        include_once $this->config->PATH_PREFIX.'objects/Photo.php';
        $this->tableName = 'photo';
        $this->bindList = '*';
        $this->where = ' WHERE galery_id = '.$id.' AND active = 1 AND deleted = 0 ';
        $this->orderBy = ' ORDER BY id ASC';
        $list = $this->select('Photo');

        return $list;
    }

    public function getLastTenPhotoList(){

        include_once $this->config->PATH_PREFIX.'objects/Photo.php';
        $this->tableName = 'photo';
        $this->bindList = '*';
        $this->where = ' WHERE active = 1 AND deleted = 0 ';
        $this->orderBy = ' ORDER BY id DESC ';
        $this->limit = ' LIMIT 0, 10 ';

        return $this->select('Photo');
    }

    public function getLastPhoto(){

        include_once $this->config->PATH_PREFIX.'objects/Photo.php';
        $this->tableName = 'photo';
        $this->bindList = '*';
        $this->where = ' WHERE active = 1 AND deleted = 0 ';
        $this->orderBy = ' ORDER BY id DESC ';
        $this->limit = ' LIMIT 0, 1 ';

        return $this->select('Photo');
    }

    /***** Blog ******************************************************************************************************************************************/

    public function getBlogList(){

        include_once $this->config->PATH_PREFIX.'objects/Blog.php';
        $this->tableName = 'blog';
        $this->bindList = '*';
        $this->where = " WHERE active = 1 AND deleted = 0 ";
        $list = $this->select('Blog');
        return $list;
    }

    public function getBlogById($id){

        include_once $this->config->PATH_PREFIX.'objects/Blog.php';
        $this->tableName = 'blog';
        $this->bindList = '*';
        $this->where = " WHERE id = ".$id." AND active = 1 AND deleted = 0 ";
        $list = $this->select('Blog');
        $blog = $list[0];
        return $blog;
    }


    /***** Tartalom ******************************************************************************************************************************************/

    public function getTartalomByLink($csana){

        include_once $this->config->PATH_PREFIX.'objects/Content.php';
        $this->tableName = 'content';
        $this->bindList = '*';
        $this->where = " WHERE link LIKE '%".$csana."%' AND active = 1 AND deleted = 0 ";
        $list = $this->select('Content');
        $content = $list[0];
        return $content->getText();
    }

}