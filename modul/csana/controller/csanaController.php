<?php

Class csanaController Extends baseController {

    public $twig = null;

    public function index() {


        $this->model->setTemplate('csana_index.twig');
        $this->initialize();
        $lastTenPhotoList =$this->model->getLastTenPhotoList();
        $this->model->assignedList->assign('lastTenPhotoList', $lastTenPhotoList);
        $this->showPage();
    }

    public function blog() {

        /*$this->model->setTemplate('blog_index.twig');
        $this->initialize();
        $blogList = $this->model->getBlogList();
        $id = $blogList[0]['id'];
        if($this->config->id > 0){
            $id = $this->config->id;
        }
        $selectedBlog = $this->model->getBlogById($id);
        $selectedPhotoList = $this->model->getPhotoListByGaleryID($selectedBlog['id']);
        $lastTenPhotoList =$this->model->getLastTenPhotoList();
        $this->model->assignedList->assign('blogList', $blogList);
        $this->model->assignedList->assign('selectedBlog', $selectedBlog);
        $this->model->assignedList->assign('selectedPhotoList', $selectedPhotoList);
        $this->model->assignedList->assign('lastTenPhotoList', $lastTenPhotoList);
        $this->showPage();
        $selectedBlog->setViews($selectedBlog->getViews()+1);
        $selectedBlog->update($this->model->pdo);*/

        /*for($i=1; $i<11; $i++){
            $sql = "INSERT INTO  `photo` (
                `id` ,
                `title` ,
                `src` ,
                `create_date` ,
                `galery_id` ,
                `active` ,
                `deleted`
                )
                VALUES (
                '0',  'kep_".$i.".jpg',  'pic/galeria/b-mindennapok/kep_".$i.".jpg',  '2016-03-16 03:45:14',  '47',  '1',  '0'
                )";

            $sth = $this->model->pdo->prepare($sql);
            $sth->execute();
        }*/
    }

    public function galery() {

        $this->model->setTemplate('galery_index.twig');
        $this->initialize();
        $galeryCategoryId = 2;
        switch ($this->csana) {
            case "halacska" : $galeryCategoryId = 2; break;
            case "madarka" : $galeryCategoryId = 2; break;
            case "ozike" : $galeryCategoryId = 3; break;
            case "pillango" : $galeryCategoryId = 3; break;
            default : $galeryCategoryId = 2; break;
        }
        $galeryList = $this->model->getGaleryListByCategoryId($galeryCategoryId);
        $fullGaleryList = array();

        $filteredGaleryList = array();
        for($i=0; $i < count($galeryList); $i++){
            $j = $i+1;
            $filter = 'flt_'.$j;
            $fullGaleryList[$i]['galery'] = $galeryList[$i];
            $fullGaleryList[$i]['galeryFilter']= $filter;
            $photoList = $this->model->getPhotoListByGaleryID($galeryList[$i]->getId());
            $fullGaleryList[$i]['photoList'] = $photoList;
            $fullGaleryList[$i]['firstPhoto'] = $photoList[0];
            $filteredGaleryList[$i]['name'] = $galeryList[$i]->getTitle();

            $filteredGaleryList[$i]['filter'] = $filter;
        }

        $lastTenPhotoList =$this->model->getLastTenPhotoList();
        $this->model->assignedList->assign('fullGaleryList', $fullGaleryList);
        $this->model->assignedList->assign('lastTenPhotoList', $lastTenPhotoList);
        $this->model->assignedList->assign('lastPhoto', $this->model->getLastPhoto());
        ChromePhp::log($this->model->getLastPhoto());
        $this->showPage();
        $this->initGaleryMenu($filteredGaleryList);

    }

    public function initGaleryMenu($galeryList){

        ChromePhp::log($galeryList);
        $galeryMenu = '<ul><li class="squareButton sb_filter active"><a href="#" data-filter="*">Mind</a></li>';
        for( $i = 0; $i <count($galeryList); $i++){
            $galeryName = $galeryList[$i]['name'];
            $galeryFilter = $galeryList[$i]['filter'];
            if($i == 0){
                $galeryMenu .= '<li class="squareButton sb_filter"><a href="#" data-filter=".'.$galeryFilter.'">'.$galeryName.'</a></li>';
            }else{
                $galeryMenu .= '<li class="squareButton sb_filter"><a href="#" data-filter=".'.$galeryFilter.'">'.$galeryName.'</a></li>';
            }

        }
        $galeryMenu .= '</ul>';

        print ' <script type="text/javascript">
                    if (jQuery("#style_portfolio_mc").length > 0) {
                        jQuery(".isotopeFiltr", "#style_portfolio_mc").append(\''.$galeryMenu.'\');
                       }
                </script>';


    }

}

?>
