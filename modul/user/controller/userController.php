<?php

Class userController Extends baseController {

    public $twig = null;
    public $facebook = null;
    public function index() {


    }

    public function login() {

        $email_not_specified = 'Please type a valid email';
        $password_not_specified = 'Please type a vaild password';
        $login_success = '{"error":"", "message":"Sikeres bejelentkezés!"}';
        $login_error = '{"error":"1", "message":"Hibás e-mail cím vagy jelszó!"}';
        $server_not_configured = '{"error":"Nincs ilyen felhasználó...!"}';

        ///////////////////////////
        //Login Form Processing//
        ///////////////////////////
        $errors = array();
        if(isset($this->config->login_email) and isset($this->config->login_pwd)) {
            if(!empty($this->config->login_email))
                $login_email = stripslashes(strip_tags(trim($this->config->login_email)));

            if(!empty($this->config->login_pwd))
                $login_pwd = stripslashes(strip_tags(trim($this->config->login_pwd)));


            //Message if no login email was specified
            if(empty($login_email)) {
                $errors[] = $email_not_specified;
            }

            //Message if no login password was specified
            if(empty($login_pwd)) {
                $errors[] = $password_not_specified;
            }

            //Login process
            if(empty($errors)) {
                $this->model->login($login_email, $login_pwd, $this->config->login_admin);
                $result_user = $this->model->user;
                if (null != $result_user) {
                    $_SESSION['loggedUserId'] = $result_user->getId();
                    $_SESSION['logged'] = 'LOGGED';
                    echo $login_success;
                } else {
                    $errors[] = $login_error;
                    echo implode('<br>', $errors );
                }
            } else {
                echo implode('<br>', $errors );
            }
        } else {
            echo '"login_email" and "login_pwd" variables were not received by server. Please check "login_email", "login_pwd" attributes for your input fields';
        }
    }


    public function getFacebookLoginUrl(){
        include 'application/Facebook.class.php';
        $this->facebook = new facebook();
        echo $this->facebook->__getFacebookLoginUrl();
    }

    public function facebookLogin(){
        $this->facebook->__facebookLogin();
        $this->model->user = $this->facebook->__getFacebookUser();
        /************  insert or update user data ***********/
        // You can redirect them to a members-only page.
        header('Location: http://kistemplomicsana.hu/');
    }

    public function logout() {
        $url = $this->config->DOMAIN.'/'.$this->config->modul.'/blog/';
        $logout_success = '{"error":"", "message":"Sikeres kijelentkezés!", "url":"'.$url.'"}';
        unset($_SESSION['logged']);
        unset($_SESSION['email']);
        echo $logout_success;
        header("location: index.php");
    }

    public function signUp() {

        $name_not_specified = 'Please type a valid name';
        $email_not_specified = 'Please type a valid email';
        $password_not_specified = 'Please type a vaild password';
        $signUp_success = '{"error":"", "message":"Sikeres Regisztráció!"}';
        $signUp_error = '{"error":"1", "message":"A megadott e-mail cím már szerepel az adatbázisban"}';
        $signUp_email_error = '{"error":"2", "message":"A megadott e-mail cím nem szerepel a szülői e-mail cím listán"}';

        ///////////////////////////
        //SignUp Form Processing//
        ///////////////////////////
        $errors = array();
        if(isset($this->config->signup_username) and isset($this->config->signup_email) and isset($this->config->signup_pwd)) {

            if(!empty($this->config->signup_username))
                $signup_username = stripslashes(strip_tags(trim($this->config->signup_username)));

            if(!empty($this->config->signup_email))
                $signup_email = stripslashes(strip_tags(trim($this->config->signup_email)));

            if(!empty($this->config->signup_pwd))
                $signup_pwd = stripslashes(strip_tags(trim($this->config->signup_pwd)));

            //Name if no SignUp name was specified
            if(empty($signup_username)) {
                $errors[] = $name_not_specified;
            }

            //E-mail if no SignUp e-mail was specified
            if(empty($signup_email)) {
                $errors[] = $email_not_specified;
            }

            //Password if no SignUp password was specified
            if(empty($signup_pwd)) {
                $errors[] = $password_not_specified;
            }

            //SignUp process
            if(empty($errors)) {

                if($this->model->email_is_accepted($signup_email)){

                    $result_user = $this->model->getUserByEmail($signup_email);
                    if (null == $result_user) {
                        $newUser = new User();
                        $newUser->setName($signup_username);
                        $newUser->setEmail($signup_email);
                        $newUser->setPassword($this->model->passwordGenerator($signup_pwd));
                        $newUser->setRegDate(date("Y-m-d H:i:s"));
                        $newUser->setConfirmationDate(date("Y-m-d H:i:s"));
                        $newUser->setLastLogin(date("Y-m-d H:i:s"));

                        $newUser->insert($this->model->pdo);
                        $_SESSION['loggedUserId'] = $newUser->getLastId();
                        $_SESSION['logged'] = 'LOGGED';
                        echo $signUp_success;
                    } else {
                        $errors[] = $signUp_error;
                        echo implode('<br>', $errors );
                    }
                }else {
                    $errors[] = $signUp_email_error;
                    echo implode('<br>', $errors );
                }

            } else {
                echo implode('<br>', $errors );
            }
        } else {
            echo '"login_email" and "login_pwd" variables were not received by server. Please check "login_email", "login_pwd" attributes for your input fields';
        }

    }

    public function sendMail(){

        $default_subject = 'Üzenet a honlap felületről';
        $name_not_specified = 'Please type a valid name';
        $message_not_specified = 'Please type a vaild message';
        $email_was_sent = '{"error":"", "message":"Sikeres üzenet küldés!"}';
        $server_not_configured = '{"error":"1", "message":"Szerver hiba miatt, az üzenet küldése nem sikerült!"}';

        ///////////////////////////
        //Contact Form Processing//
        ///////////////////////////
        $errors = array();
        if(isset($this->config->message) and isset($this->config->sender_name)) {
            if(!empty($this->config->sender_name))
                $sender_name = stripslashes(strip_tags(trim($this->config->sender_name)));

            if(!empty($this->config->message))
                $message = stripslashes(strip_tags(trim($this->config->message)));

            if(!empty($this->config->sender_email))
                $sender_email = stripslashes(strip_tags(trim($this->config->sender_email)));

            if(!empty($this->config->subject))
                $subject = stripslashes(strip_tags(trim($this->config->subject)));


            //Message if no sender name was specified
            if(empty($sender_name)) {
                $errors[] = $name_not_specified;
            }

            //Message if no message was specified
            if(empty($message)) {
                $errors[] = $message_not_specified;
            }

            $subject = (!empty($subject)) ? $subject : $default_subject;
            $message = (!empty($message)) ? wordwrap($message, 70) : '';

            //sending message if no errors
            if(empty($errors)) {
                if ($this->model->sendHTMLEmail($sender_name, $sender_email, $subject, $message, $this->config->EMAIL)) {
                    echo $email_was_sent;
                } else {
                    $errors[] = $server_not_configured;
                    echo implode('<br>', $errors );
                }
            } else {
                echo implode('<br>', $errors );
            }
        } else {
            echo '"name" and "message" variables were not received by server. Please check "name" attributes for your input fields';
        }
    }

}

?>
