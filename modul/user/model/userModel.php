<?php

/**
 * Created by PhpStorm.
 * User: Ouechtati Saber
 * Date: 2015.10.04.
 * Time: 15:40
 */

class userModel extends baseModel {



     public function login($email, $password, $admin=false){

         include_once $this->config->PATH_PREFIX.'objects/User.php';
         $password = $this->passwordGenerator($password);
         $this->tableName = 'user';
         $this->bindList = '*';
         if($admin){
             $this->where = "WHERE email = '{$email}' AND password = '{$password}' AND type = 1 ";
         }else{
             $this->where = "WHERE email = '{$email}' AND password = '{$password}' ";
         }
         $this->limit = 'LIMIT 1';
         $result = $this->select('User');
         $this->user = $result[0];
     }

    public function email_is_accepted($email){
        include_once $this->config->PATH_PREFIX.'objects/Email_list.php';
        $this->tableName = 'email_list';
        $this->bindList = '*';
        $this->where = "WHERE email = '{$email}' AND deleted = 0 ";
        //$this->limit = 'LIMIT 1 ';
        $result = $this->select('email_list');
        if(null != $result[0]){
            return true;
        }else{
            return false;
        }
    }

}