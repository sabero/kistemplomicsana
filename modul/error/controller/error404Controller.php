<?php

Class error404Controller Extends baseController {

        public function index()
        {
                $csana = $this->config->CSANA;
                $Csana = "Bárány";

                switch ($csana) {
                        case "halacska" : $Csana = "Halacska"; break;
                        case "madarka" : $Csana = "Madárka"; break;
                        case "ozike" : $Csana = "Őzike"; break;
                        case "pillango" : $Csana = "Pillango"; break;
                        default : $Csana = "Bárány"; break;
                }

                $SiteMapItems = $this->model->getSiteMapItemList();
                $csanaContent = $csana."_content.twig";

                $this->loadTwig($this->config->MODUL);
                $this->twig->display('error_index.twig', array('DOMAIN' => $this->config->DOMAIN, 'action' => $this->config->ACTION, 'csana' => $csana, 'Csana' => $Csana, 'SitemapItems' => $SiteMapItems, 'csanaContent' => $csanaContent,'class' => 'page404', 'titleError' => '404', 'h2_title' => 'A keresett oldal nem található!'));
        }


}
?>
