<?php

/**
 * Created by PhpStorm.
 * User: Ouechtati Saber
 * Date: 2015.10.04.
 * Time: 15:40
 */

class errorModel extends baseModel {

    public function getSiteMapItemList(){

        $items = array();
        $items[0]['name'] = 'Bárány';
        $items[1]['name'] = 'Halacska';
        $items[2]['name'] = 'Madárka';
        $items[3]['name'] = 'Őzike';
        $items[4]['name'] = 'Pillango';

        $items[0]['_name'] = 'barany';
        $items[1]['_name'] = 'halacska';
        $items[2]['_name'] = 'madarka';
        $items[3]['_name'] = 'ozike';
        $items[4]['_name'] = 'pillango';

        return $items;
    }

}