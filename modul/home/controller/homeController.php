<?php

Class homeController Extends baseController {

    public $twig = null;

    public function index() {

        $this->loadTwig($this->config->MODUL);
        $this->twig->display('home_index.twig', array('DOMAIN' => $this->config->DOMAIN));

    }

}

?>
