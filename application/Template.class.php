<?php

Class Template {

	private $registry;
	private $vars = array();

	/**
	 * @constructor
	 * @param $registry
	 */
	function __construct($registry) {
		$this->registry = $registry;

	}


	 /**
	 * @set undefined vars
	 * @param string $index
	 * @param mixed $value
	 * @return void
	 */
	 public function __set($index, $value)
	 {
			$this->vars[$index] = $value;
	 }


	function show($modul, $name) {
		$path = '../modul/' . $modul . '/views' . '/' . $name . '.twig';

		if (file_exists($path) == false)
		{
			throw new Exception('Template not found in '. $path);
			return false;
		}

		// Load variables
		foreach ($this->vars as $key => $value)
		{
			$$key = $value;
		}

		include ($path);
	}


}

?>
