<?php

Class Facebook {

	private $fb;

	/**
	 * @constructor
	 * @param $registry
	 */
	function __construct() {
		/** require the facebook sdk version 5.0.0 for php */
		require_once 'facebook/src/Facebook/autoload.php';
		$this->fb = new Facebook\Facebook([
			'app_id' => '900411903382301',
			'app_secret' => 'c7243162afaa99bf0ae541fa9d725ae6',
			'default_graph_version' => 'v2.4',
		]);

	}


	//Get facebook login url
	public function __getFacebookLoginUrl(){

		$helper = $this->fb->getRedirectLoginHelper();

		$permissions = ['email']; // Optional permissions
		$loginUrl = $helper->getLoginUrl('http://kistemplomicsana.hu/', $permissions);

		return htmlspecialchars($loginUrl);
	}

	//facebook login
	public function __facebookLogin(){

		/** require the facebook sdk version 5.0.0 for php */
		require_once 'facebook/src/Facebook/autoload.php';

		$error = "";

		$helper = $this->fb->getRedirectLoginHelper();

		try {
			$accessToken = $helper->getAccessToken();
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
			// When Graph returns an error
			$error = 'Graph returned an error: ' . $e->getMessage();
			exit;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
			// When validation fails or other local issues
			$error =  'Facebook SDK returned an error: ' . $e->getMessage();
			exit;
		}

		if (! isset($accessToken)) {
			if ($helper->getError()) {
				header('HTTP/1.0 401 Unauthorized');
				$error =  "Error: " . $helper->getError() . "\n";
				$error .=  "Error Code: " . $helper->getErrorCode() . "\n";
				$error .=  "Error Reason: " . $helper->getErrorReason() . "\n";
				$error .=  "Error Description: " . $helper->getErrorDescription() . "\n";
			} else {
				header('HTTP/1.0 400 Bad Request');
				$error =  'Bad request';
			}
			exit;
		}

		// Logged in
		echo '<h3>Access Token</h3>';
		var_dump($accessToken->getValue());

		// The OAuth 2.0 client handler helps us manage access tokens
		$oAuth2Client = $this->fb->getOAuth2Client();

		// Get the access token metadata from /debug_token
		$tokenMetadata = $oAuth2Client->debugToken($accessToken);
		echo '<h3>Metadata</h3>';
		var_dump($tokenMetadata);

		// Validation (these will throw FacebookSDKException's when they fail)
		$tokenMetadata->validateAppId('900411903382301');
		// If you know the user ID this access token belongs to, you can validate it here
		// $tokenMetadata->validateUserId('123');
		$tokenMetadata->validateExpiration();

		if (! $accessToken->isLongLived()) {
			// Exchanges a short-lived access token for a long-lived one
			try {
				$accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
			} catch (Facebook\Exceptions\FacebookSDKException $e) {
				$error = "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>";
				exit;
			}
			echo '<h3>Long-lived</h3>';
			var_dump($accessToken->getValue());
		}

		$_SESSION['fb_access_token'] = (string) $accessToken;

		// User is logged in with a long-lived access token.

		// You can redirect them to a members-only page.
		//header('Location: http://kistemplomicsana.hu/');

	}

	public function __getFacebookUser(){

		$fbUser = new User();

		try {
			// Returns a `Facebook\FacebookResponse` object
			$response = $this->fb->get('/me?fields=id,name', '{access-token}');
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
			echo 'Graph returned an error: ' . $e->getMessage();
			exit;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
			echo 'Facebook SDK returned an error: ' . $e->getMessage();
			exit;
		}

		$graphUser = $response->getGraphUser();
		$fbUser->setUserName($graphUser->getName());
		$fbUser->setUserEmail($graphUser['email']);
		//ide j�nn a t�bbi mez�...

		//echo 'Name: ' . $user['name'];
		// OR
		// echo 'Name: ' . $user->getName();

		return$fbUser;
	}

	/**
	 * @return \Facebook\Facebook
	 */
	public function getFb()
	{
		return $this->fb;
	}

	/**
	 * @param \Facebook\Facebook $fb
	 */
	public function setFb($fb)
	{
		$this->fb = $fb;
	}



}

?>
