<?php

/**
 * Created by PhpStorm.
 * User: Ouechtati Saber
 * Date: 2015.10.09.
 * Time: 22:37
 */



Class BaseModel
{

	/*
     * @registry object
     */
	protected $registry;
	protected $config;
	public $pdo;
	public $tableName='';
	public $bindList='*';
	public $where='WHERE 1';
	public $join='';
	public $groupBy='';
	public $orderBy='';
	public $limit='';
	public $user;

	public $assignedList;
	private $template;


	function __construct($registry, $config)
	{
		$this->registry = $registry;
		$this->config = $config;
		$this->pdo = $this->registry->db;
		$this->pdo->exec("SET CHARACTER SET utf8");

		include_once $this->config->PATH_PREFIX.'objects/User.php';
		$this->user = new User();
		include_once $this->config->PATH_PREFIX.'objects/AssignedList.php';
		$this->assignedList = new AssignedList();
	}

	public function select($className){

		try
		{
			if(null != $this->pdo){
				$query = 'SELECT '.$this->bindList.' FROM '.$this->tableName.' '.$this->join.' '.$this->where.' '.$this->orderBy.' '.$this->limit;
				$result = $this->pdo->query($query);
				$result->setFetchMode(PDO::FETCH_CLASS, $className);
				$objectList = $result->fetchAll();
				/*$objectList = array();
				$index = 0;
				while($object = $result->fetch()) {
					$objectList[$index] = $object->toJson();
					$index++;
				}*/

				//ChromePhp::log($query);
				//ChromePhp::log($objectList);

				return $objectList;
			}else{
				return null;
			}

		}catch (PDOException $e){
			ChromePhp::log($e->getMessage());
			return null;
		}
	}

	public function getUserById($user_id){

		include_once $this->config->PATH_PREFIX.'objects/User.php';
		$this->tableName = 'user';
		$this->bindList = '*';
		$this->where = " WHERE id = {$user_id} ";
		//$this->limit = ' LIMIT 1';
		$result = $this->select('User');
		return $result[0];
	}

	public function getObjectById($class, $id){

		include_once $this->config->PATH_PREFIX.'objects/'.$class.'.php';
		$this->tableName = strtolower($class);
		$this->bindList = '*';
		$this->where = " WHERE id = {$id} ";
		//$this->limit = ' LIMIT 1';
		$result = $this->select($class);
		return $result[0];
	}

	public function getUserByEmail($email){

		include_once $this->config->PATH_PREFIX.'objects/User.php';
		$this->tableName = 'user';
		$this->bindList = '*';
		$this->where = " WHERE email = '{$email}' AND deleted = 0 ";
		//$this->limit = ' LIMIT 1';
		$result = $this->select('User');
		return $result[0];
	}

	public function passwordGenerator($pass){
		$salt = md5("https://localhost/application/86pgkm;nokplg/a9(D],;o;p'5;j%;k");
		return sha1(substr(md5($pass),1,19).strlen($salt) . $salt) . substr($salt,2,7);
	}

	public function sendHTMLEmail($nev, $email, $targy, $uzenet, $cimzett){

		$message = '<html><body style=" font-size: 1.3em; padding:50px;">';
		$message .= '<div style="padding:50px; color: #ffffff;  background: #bb22bb; border-radius: 20px;"> ';
		$message .= '<div  style="padding-bottom:70px;" >';
		$message .= '<p>Kistemplomicsana.hu honlapr�l be�rkezett �zenet <br>';
		$message .= 'K�ldte: <b>'.$nev.'</b><br>';
		$message .= ' T�rgy: <b>'.$targy.'</b></p>';
		$message .= '</div>';
		$message .= '<div  style="padding-bottom:50px;" >';
		$message .= '<p> �zenet: </p>';
		$message .= '<div style="padding:50px; color: #ffffff;  background: #bb55bb; border-radius: 20px;"><p>' .$uzenet. '</p></div>';
		$message .= '</div>';
		$message .= '<div> <img src="http://kistemplomicsana.hu/pic/bg/texture_14.png"></div>';
		$message .= '</div></body></html>';

		$subject = $targy;
		$headers = "From: ".$nev."<".$email.">\r\n";
		$headers .= "Reply-To: ".$email." \r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=utf-8\r\n";

		if(mail($cimzett, $subject, $message, $headers, '-f'. $email)){
			return true;
		}else{
			return false;
		}

	}

	/**
	 * @return mixed
	 */
	public function getTemplate()
	{
		return $this->template;
	}

	/**
	 * @param mixed $template
	 */
	public function setTemplate($template)
	{
		$this->template = $template;
	}

}

?>
