<?php

Class Init
{

    public function __construct($pathPrefix)
    {
        /*** include the controller class ***/
        include_once $pathPrefix.'application/Controller_base.class.php';

        /*** include the controller class ***/
        include_once $pathPrefix.'application/Model_base.class.php';

        /*** include $pathPrefix.the registry class ***/
        include_once $pathPrefix.'application/Registry.class.php';

        /*** include the router class ***/
        include_once $pathPrefix.'application/Router.class.php';

        /*** include the template class ***/
        include_once $pathPrefix.'application/Template.class.php';

        /*** include the config class ***/
        include_once $pathPrefix.'application/Config.class.php';

        /*** include the template class ***/
        include_once $pathPrefix.'application/Db.class.php';

        /*** a new registry object ***/
        $registry = new Registry();

        /*** host (0: host = localehost)***/
        $host = 0;

        /*** a new config object ***/
        $config = new Config($host, $pathPrefix);

        /*** create the database registry object ***/
        $registry->db = Db::getInstance($config->dbName, $config->dbUser, $config->dbPass);

    }

}

?>