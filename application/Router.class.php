<?php

class Router {

	 private $registry;
	 private $config;
	 private $path;
	 private $args = array();
	 public $controllerFile;
	 public $modelFile;
	 public $controller;
	 public $csana;
	 public $modul;
	 public $action;
	 public $model;
	/**
	 * Constructor
	 * @param $registry
	 * @param $config
	 */
	 function __construct($registry, $config) {
		 $this->registry = $registry;
		 $this->config = $config;
	 }


	/**
	 * @set controller directory path
	 * @param string $path
	 * @param void $modul
	 * @throws Exception
	 */
	 function setPath($path, $csana, $modul, $controller, $action, $model) {

		/*** check if path i sa directory ***/
		if (is_dir($path) == false)
		{
			throw new Exception ('Invalid controller path: `' . $path . '`');
		}
		/*** set the path ***/
		$this->path = $path;
		$this->modul = $modul;
		$this->controller = $controller;
		$this->action = $action;
		$this->model = $model;
		$this->config->CSANA = $csana;
		$this->config->MODUL = $modul;
		$this->config->CONTROLLER = $controller;
		$this->config->ACTION = $action;
		$this->config->MODEL = $model;
	 }


	 /**
	 * @load the controller
	 * @access public
	 * @return void
	 */
	 public function controllerLoader()
	 {
		$this->getController();
		$this->getModel();

		/*** if the controllerFile or modelFile is not there diaf ***/
		if ((is_readable($this->controllerFile) == false) || (is_readable($this->modelFile) == false))
		{
			//$this->controllerFile = $this->path.'/modul/error/error404Controller.php';
			//$this->controller = 'error404Controller';
		}

		/*** include the controller ***/
		include $this->controllerFile;

		 /*** include the model ***/
		 include $this->modelFile;

		 /*** a new model class instance ***/
		 $modelClass = $this->model;
		 $model = new $modelClass($this->registry, $this->config);

		/*** a new controller class instance ***/
		$controllerClass = $this->controller;
		$controller = new $controllerClass($this->registry, $this->config, $model);

		/*** check if the action is callable ***/
		if (is_callable(array($controller, $this->action)) == false)
		{
			$action = 'index';
		}
		else
		{
			$action = $this->action;
		}
		/*** run the action ***/
		$controller->$action();
	 }


	 /**
	 * @get the controller
	 * @access private
	 * @return void
	 */
	private function getController()
	{
		/*** set the file path ***/
		$this->controllerFile = $this->path . 'controller/' . $this->controller .'.php';
	}

	/**
	 * @get the model
	 * @access private
	 * @return void
	 */
	private function getModel()
	{
		/*** set the file path ***/
		$this->modelFile = $this->path . 'model/' . $this->model .'.php';
	}
}

?>
