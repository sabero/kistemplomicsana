<?php

Abstract Class BaseController
{

	/*
     * @registry object
     */
	protected $registry;
	protected $config;
	protected $model;
	protected $csana;


	function __construct($registry, $config, $model)
	{
		$this->registry = $registry;
		$this->config = $config;
		$this->model = $model;
	}

	/**
	 * @all controllers must contain an index method
	 */
	abstract function index();

	public function initialize(){

		if($this->config->SITE_TYPE != 'admin'){
			$this->csana = $this->config->CSANA;
			$Csana = "Bárány";

			switch ($this->csana) {
				case "halacska" : $Csana = "Halacska"; break;
				case "madarka" : $Csana = "Madárka"; break;
				case "ozike" : $Csana = "Őzike"; break;
				case "pillango" : $Csana = "Pillangó"; break;
				default : $Csana = "Bárány"; break;
			}

			$SiteMapItems = $this->model->getSiteMapItemList();
			$csanaContentTemplate = $this->csana."_content.twig";
			$tartalom = $this->model->getTartalomByLink($this->csana);


			$this->model->assignedList->assign('csana', $this->csana);
			$this->model->assignedList->assign('Csana', $Csana);
			$this->model->assignedList->assign('SitemapItems', $SiteMapItems);
			$this->model->assignedList->assign('csanaContent', $csanaContentTemplate);
			$this->model->assignedList->assign('tartalom', $tartalom);
		}

		$loggedUser = $this->model->getUserById($this->config->loggedUserId);
		$this->model->assignedList->assign('LOGGED', $this->config->logged);
		$this->model->assignedList->assign('LoggedUser', $loggedUser);
		$this->model->assignedList->assign('DOMAIN', $this->config->DOMAIN);
		$this->model->assignedList->assign('ADMIN_DOMAIN', $this->config->ADMIN_DOMAIN);
		$this->model->assignedList->assign('action', $this->config->ACTION);
	}

	public function loadTwig($modul)
	{
		$autoloaderFile = $this->config->PATH_PREFIX.'twig/lib/Twig/Autoloader.php';
		require_once $autoloaderFile;
		Twig_Autoloader::register();
		$twigLoader = new Twig_Loader_Filesystem(
			array($this->config->PATH_PREFIX.'modul/'.$modul.'/views/', $this->config->PATH_PREFIX.'includes/')
		);
		// set up environment
		$params = array(
			'cache' => 'cache',
			'auto_reload' => true, // disable cache
			'autoescape' => true
		);
		$this->twig = new Twig_Environment($twigLoader, $params);
	}

	public function showPage(){
		$this->loadTwig($this->config->MODUL);
		$this->twig->display($this->model->getTemplate(), $this->model->assignedList->getList());
	}


}

?>
